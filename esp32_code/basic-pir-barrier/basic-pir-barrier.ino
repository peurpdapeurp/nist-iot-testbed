
#define PIN_LED 25
#define PIN_PIR1 18
#define PIN_PIR2 27

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_PIR1, INPUT);
  pinMode(PIN_PIR2, INPUT);
}

int lastModePIR1 = 0;
int lastModePIR2 = 0;

int motionWaitTime = 3000;
int motionStartTimer = 0;

bool checkForBuildingEntry = false;
bool checkForBuildingExit = false;

// returns 0 if no change from last state, 1 if motion detected, -1 if motion stopped
int
motionCheck(int pirPin, int &lastMode) {
  if (digitalRead(pirPin) == HIGH && lastMode == 0) {
    lastMode = 1;
    return 1;
  }
  else if (digitalRead(pirPin) == LOW && lastMode == 1) {
    lastMode = 0;
    return -1;
  }

  return 0;
}

int currentOccupancy = 0;

void
loop()
{
  int currentMotionPIR1 = motionCheck(PIN_PIR1, lastModePIR1);
  int currentMotionPIR2 = motionCheck(PIN_PIR2, lastModePIR2);
  
  if (currentMotionPIR1 == 1) {
    Serial.println("Motion started for pir 1.");
    if (!checkForBuildingEntry && !checkForBuildingExit)
      checkForBuildingEntry = true;
  }
  else if (currentMotionPIR1 == -1) {
    Serial.println("Motion stopped for pir 1.");
  }
  
  if (currentMotionPIR2 == 1) {
    Serial.println("Motion started for pir 2.");
    if (!checkForBuildingEntry && !checkForBuildingExit)
      checkForBuildingExit = true;
  }
  else if (currentMotionPIR2 == -1) {
    Serial.println("Motion stopped for pir 2.");
  }

  if (checkForBuildingEntry) {
    if (motionStartTimer == 0) {
      motionStartTimer = millis();
    }
    else if (millis() < motionStartTimer + motionWaitTime) {
      if (currentMotionPIR2 == 1) {
        Serial.println("Someone entered the building.");
        motionStartTimer = 0;
        currentOccupancy++;
        Serial.print("Current building occupancy: ");
        Serial.println(currentOccupancy);
        checkForBuildingEntry = false;
      }
    }
    else {
      Serial.println("Motion wait time ran out, ignoring building entry starting motion.");
      motionStartTimer = 0;
      checkForBuildingEntry = false;
    }
  }

  if (checkForBuildingExit) {
    if (motionStartTimer == 0) {
      motionStartTimer = millis();
    }
    else if (millis() < motionStartTimer + motionWaitTime) {
      if (currentMotionPIR1 == 1) {
        Serial.println("Someone exited the building.");
        motionStartTimer = 0;
        currentOccupancy--;
        Serial.print("Current building occupancy: ");
        Serial.println(currentOccupancy);
        checkForBuildingExit = false;
      }
    }
    else {
      Serial.println("Motion wait time ran out, ignoring building exit starting motion.");
      motionStartTimer = 0;
      checkForBuildingExit = false;
    }
  }

}



