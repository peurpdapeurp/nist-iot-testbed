#include <cstring>

#include <Adafruit_BME680.h>
#include <esp8266ndn.h>
#include <ndn-cpp/c/encoding/tlv/tlv-decoder.h>
#include <PString.h>
#include <Streaming.h>
#include <U8g2lib.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <math.h>
#include <security/micro-ecc/uECC.h>

#define PIN_BTN 0
#define PIN_LED 25
#define PIN_PIR1 18
#define PIN_PIR2 27
#define WIFI_SSID "EdwardPi"
#define WIFI_PASS "11111111"
#define IP_CLIENT IPAddress(192, 168, 4, 8)
#define IP_SERVER IPAddress(192, 168, 4, 1)
#define IP_SUBNET IPAddress(255, 255, 255, 0)
#define UDP_PORT 6363

Adafruit_BME680 g_bme;
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, 15, 4, 16);

boolean is_onboarded = false;

WiFiUDP g_udp;
ndn::UnicastUdpTransport g_transport(g_udp);
ndn::Face g_face(g_transport);

#define PREFIX_NCOMPS 10
char COMMAND_PREFIX[] = "/device/command/ESP32ID001";
ndn_NameComponent g_command_prefixComps[PREFIX_NCOMPS];
ndn::NameLite g_command_prefix(g_command_prefixComps, PREFIX_NCOMPS);
#define PAYLOAD_BUFSIZE 256

//char CONTROLLER_PREFIX[] = "/NIST/library/mainroom/repo1/proxy/onboard";
char BOOTSTRAP_PREFIX[] = "/ndn/sign-on";
ndn_NameComponent bootstrap_prefixComps[PREFIX_NCOMPS];
ndn::NameLite bootstrap_prefix(bootstrap_prefixComps, PREFIX_NCOMPS);

char CERT_PREFIX[] = "/ucla/eiv396";
ndn_NameComponent cert_prefixComps[PREFIX_NCOMPS];
ndn::NameLite cert_prefix(cert_prefixComps, PREFIX_NCOMPS);

char temperatureString[] = "temperature";
ndn_NameComponent temperature_nameComps[1];
ndn::NameLite temperature_name(temperature_nameComps, 1);

char humidityString[] = "humidity";
ndn_NameComponent humidity_nameComps[1];
ndn::NameLite humidity_name(humidity_nameComps, 1);

char airPressureString[] = "pressure";
ndn_NameComponent airPressure_nameComps[1];
ndn::NameLite airPressure_name(airPressure_nameComps, 1);

char gasResistanceString[] = "resistance";
ndn_NameComponent gasResistance_nameComps[1];
ndn::NameLite gasResistance_name(gasResistance_nameComps, 1);

char occupancyString[] = "occupancy";
ndn_NameComponent occupancy_nameComps[1];
ndn::NameLite occupancy_name(occupancy_nameComps, 1);

ndn_NameComponent digest_chars_nameComps[1];
ndn::NameLite digest_chars_name(digest_chars_nameComps, 1);

//const char HMAC_SECRET[] = "334fda57-5ff7-45cc-8179-d9c83ac09530";

const uint8_t HMAC_SECRET[] = {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
  10, 11, 12, 13, 14, 15, 16, 17,
  18, 19, 20, 21, 22, 23, 24, 25,
  26, 27, 28, 29, 30, 31
};

const uint8_t BOOTSTRAP_ECC_PRIVATE[] {
  0x45, 0xF6, 0xE1, 0x48, 0x07, 0x74, 0xCF, 0x5C, 0xDC, 0x6E, 0x67, 0xC9, 0x52, 0xAE,
  0x8B, 0x17, 0x69, 0xE9, 0x6B, 0xA2, 0xF7, 0xB8, 0xEA, 0xE2, 0xEC, 0xE8, 0x80, 0xE1,
  0x7E, 0xCB, 0x99, 0x1D
};

const uint8_t BOOTSTRAP_ECC_PUBLIC[] {
  0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x02, 0x01, 0x06,
  0x08, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04, 0xAA,
  0x33, 0xDE, 0xD6, 0x5E, 0xC0, 0x7D, 0xBB, 0x40, 0x88, 0xE1, 0x79, 0x14, 0x36, 0xFD,
  0x68, 0x1E, 0xDA, 0x59, 0x0F, 0xD7, 0xDD, 0x53, 0x0B, 0x52, 0x50, 0x28, 0x52, 0x5A,
  0x37, 0x52, 0x50, 0x78, 0xDE, 0x77, 0xD8, 0x3B, 0x06, 0x83, 0xCE, 0x58, 0xD9, 0x33,
  0x5C, 0x30, 0x09, 0xFB, 0x5A, 0x41, 0xA4, 0xA7, 0xF0, 0x13, 0x42, 0xBC, 0x2E, 0xD6,
  0xA3, 0x5A, 0x1A, 0x1F, 0x91, 0xD9, 0xCF
};

uint8_t controller_trust_anchor[100];
uint32_t controller_trust_anchor_length;

uint8_t pvtkey[32];
uint8_t pubkey[64];

char digestChars[65];
uint8_t tokenArray[8];
char tokenStringArray[20];
uint8_t ndnCertificateBytes[1000];
ndn::DynamicUInt8ArrayLite certificateBytesOutput(ndnCertificateBytes, sizeof(ndnCertificateBytes), 0);
uint64_t certificateGenerationInfo[2];
uint8_t received_certificate_bytes[1000];
size_t received_certificate_size;

uint8_t ECC_PUBLIC_DIGEST[ndn_SHA256_DIGEST_SIZE];

char deviceID[] = "ESP32ID001";

//ndn::HmacKey g_pvtKey(reinterpret_cast<const uint8_t*>(HMAC_SECRET), sizeof(HMAC_SECRET));
//ndn::HmacKey g_pvtKey(HMAC_SECRET, 32);
//ndn::HmacKey& g_pubKey = g_pvtKey;

ndn_NameComponent bootstrapPvtKeyNameComps[1];
ndn::NameLite bootstrapPvtKeyName(bootstrapPvtKeyNameComps, 0);
ndn::EcPrivateKey g_bootstrapPvtKey(BOOTSTRAP_ECC_PRIVATE, bootstrapPvtKeyName);
ndn::EcPublicKey g_bootstrapPubKey(BOOTSTRAP_ECC_PUBLIC);

// so i also need to make a name component out of
// the name of my key, for now i will just name it
// /ESP32ID001/KEY/<key-id>/self/<timestamp>
ndn_NameComponent cert_nameComps[5];
ndn::NameLite cert_name(cert_nameComps, 5);

// these are declared first, the byte arrays pvtkey and pubkey are filled in during
// the bootstrapping process
ndn::EcPrivateKey g_generatedPvtKey(pvtkey, cert_name);
ndn::EcPublicKey g_generatedPubKey(pubkey);

int currentOccupancy = 0;

void
p(uint8_t X) {

  Serial.print("0x");

  if (X < 16) {
    Serial.print("0");
  }


  Serial.print(X, HEX);
  Serial.print(", ");

}

void
putInCharBuf(char *buf, int pos, uint8_t X) {

  String stringOne =  String(X, HEX);

  if (X < 16) {
    buf[pos] = '0';
    buf[pos + 1] = stringOne[0];
  }
  else {
    buf[pos] = stringOne[0];
    buf[pos + 1] = stringOne[1];
  }

}

void
printByteArray(const uint8_t *arr, int start, int end) {

  int counter = 1;
  for (int i = start; i < end; i++) {

    p(arr[i]);

    if (counter > 9) {
      counter = 0;
      Serial.println("");
    }
    counter++;
  }
  Serial.println("");

}

void
print64(uint64_t value)
{
  const int NUM_DIGITS    = log10(value) + 1;

  char sz[NUM_DIGITS + 1];

  sz[NUM_DIGITS] =  0;
  for ( size_t i = NUM_DIGITS; i--; value /= 10)
  {
    sz[i] = '0' + (value % 10);
  }

  Serial.print(sz);
}

void
loadTokenString(uint64_t value) {
  const int NUM_DIGITS    = log10(value) + 1;

  tokenStringArray[NUM_DIGITS] =  '\0';
  for ( size_t i = NUM_DIGITS; i--; value /= 10)
  {
    tokenStringArray[i] = '0' + (value % 10);
  }
}
/*
  void
  loadTokenSignatureURI(uint8_t* sig, int sigLen)
  {
  int uriStrIndex = 0;
  for (int i = 0; i < sigLen; i++) {
    uint8_t cur = sig[i];
    if ((cur >= 65 && cur <= 90) || (cur >= 97 && cur <= 122) || (cur >= 48 && cur <= 57) || cur == 45 || cur == 46 || cur == 95 || cur == 126) {
      tokenSignatureURIEncodingArray[uriStrIndex] = cur;
      uriStrIndex++;
    }
    else {
      tokenSignatureURIEncodingArray[uriStrIndex] = '%';
      uriStrIndex++;

      String hexString = String(cur, HEX);

      if (cur < 16) {
        tokenSignatureURIEncodingArray[uriStrIndex] = '0';
        uriStrIndex++;
        tokenSignatureURIEncodingArray[uriStrIndex] = hexString.c_str()[0];
      }
      else {
        tokenSignatureURIEncodingArray[uriStrIndex] = hexString.c_str()[0];
        uriStrIndex++;
        tokenSignatureURIEncodingArray[uriStrIndex] = hexString.c_str()[1];
        uriStrIndex++;
      }
    }
  }
  tokenSignatureURIEncodingArray[uriStrIndex] = '\0';
  }
*/

union ArrayToInteger {
  byte array[2];
  uint16_t integer;
};

int
getTLVLength(uint8_t *x) {
  switch (*x) {
    case 253:
      {
        ArrayToInteger converter = {x[2], x[1]};
        Serial.print("Two byte tlv length: ");
        Serial.println((int) converter.integer);
        return (int) converter.integer;
      }
    case 254:
      {
        Serial.println("GETTING TLV LENGTH OF 4 BYTES NOT IMPLEMENTED YET.");
        return 0;
      }
    case 255:
      {
        Serial.println("GETTING TLV LENGTH OF 8 BYTES NOT IMPLEMENTED YET.");
        return 0;
      }
    default:
      {
        return *x;
      }
  }
}

int
getTLVLengthEncodingLength(uint8_t x) {
  switch (x) {
    case 253:
      return 2;
    case 254:
      return 4;
    case 255:
      return 8;
    default:
      return 1;
  }
}

int
generateNDNCertificate(uint8_t* pubkey, int pubkeyLength) {

  ndn::DataLite certificate(cert_nameComps, PREFIX_NCOMPS + 1, cert_nameComps, PREFIX_NCOMPS + 1);

  certificate.getMetaInfo().setType(ndn_ContentType_KEY);

  uint8_t versionBuffer[20];
  unsigned long long currentTime = (unsigned long long) millis();
  certificate.getName().append(deviceID);
  certificate.getName().append("KEY");
  certificate.getName().append("key-id");
  certificate.getName().append("self");
  certificate.getName().appendVersion(currentTime, versionBuffer, 20);

  /*
    certificate.getSignature().getKeyLocator().getKeyName().append(deviceID);
    certificate.getSignature().getKeyLocator().getKeyName().append("KEY");
    certificate.getSignature().getKeyLocator().getKeyName().append("key-id");
    certificate.getSignature().getKeyLocator().getKeyName().append("self");
    certificate.getName().appendVersion(currentTime, versionBuffer, 20);
  */

  uint8_t ASN1EncodingBeginning[27] = {
    0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2A, 0x86, 0x48, 0xCE,
    0x3D, 0x02, 0x01, 0x06, 0x08, 0x2A, 0x86, 0x48, 0xCE, 0x3D,
    0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04,
  };

  int ASN1EncodedPubKeyBytesLength = 27 + pubkeyLength;
  int contentLength = ASN1EncodedPubKeyBytesLength;

  Serial.print("ASN1EncodedPubKeyBytesLength ");
  Serial.println(ASN1EncodedPubKeyBytesLength);
  Serial.print("contentLength: ");
  Serial.println(contentLength);

  if (contentLength > 253) {
    Serial.println("ONLY GENERATING NDN CERTIFICATES WITH CONTENT LENGTHS LESS THAN 253 IS SUPPORTED FOR NOW, SORRY");
    return -1;
  }
  uint8_t contentBytes[contentLength];
  for (int i = 0; i < 27; i++) {
    contentBytes[i] = ASN1EncodingBeginning[i];
  }
  for (int i = 27; i < contentLength; i++) {
    contentBytes[i] = pubkey[i - 27];
  }

  Serial.println("Bytes of content of certificate:");
  printByteArray(contentBytes, 0, contentLength);

  ndn::BlobLite contentBlob(contentBytes, contentLength);

  certificate.setContent(contentBlob);

  uint8_t m_outBuf[1000];
  ndn::DynamicUInt8ArrayLite m_outArr(m_outBuf, sizeof(m_outBuf), 0);
  uint8_t m_sigBuf[ndn::EcPrivateKey::MAX_SIG_LENGTH];
  ndn_Error error = g_generatedPvtKey.setSignatureInfo(certificate.getSignature());

  size_t signedBegin, signedEnd, len;
  error = ndn::Tlv0_2WireFormatLite::encodeData(certificate, &signedBegin, &signedEnd, m_outArr, &len);
  if (error) {
    Serial.println("Generated certificate encoding error: ");
    Serial.println(ndn_getErrorString(error));
    return -1;
  }

  int sigLen = g_generatedPvtKey.sign(m_outBuf + signedBegin, signedEnd - signedBegin, m_sigBuf);
  if (sigLen == 0) {
    Serial.println("Generated certificate signing error");
    Serial.println(ndn_getErrorString(error));
    return -1;
  }
  certificate.getSignature().setSignature(ndn::BlobLite(m_sigBuf, sigLen));
  error = ndn::Tlv0_2WireFormatLite::encodeData(certificate, &signedBegin, &signedEnd, m_outArr, &len);
  if (error) {
    Serial.println("Generated certificate encoding-2 error: ");
    Serial.println(ndn_getErrorString(error));
    return -1;
  }

  for (int i = 0; i < len; i++) {
    ndnCertificateBytes[i] = m_outBuf[i];
  }

  certificateGenerationInfo[0] = 0;
  certificateGenerationInfo[1] = len;

  Serial.println("Bytes of generated certificate:");
  printByteArray(ndnCertificateBytes, 0, len);
}

void
espSendBootstrappingInterest()
{
  ndn_NameComponent nameComps[10];
  ndn::InterestLite interest(nameComps, 10, nullptr, 0, nullptr, 0);
  interest.setName(bootstrap_prefix);
  //interest.getName().append(WiFi.localIP().toString().c_str());
  //interest.getName().append(deviceID);
  interest.getName().append(digestChars);
  // interest.setCanBePrefix(true);
  interest.setMustBeFresh(true);

  g_face.setSigningKey(g_bootstrapPvtKey);
  g_face.sendSignedInterest(interest);
  Serial << "<I " << ndn::PrintUri(interest.getName()) << endl;


}

union ArrayToInteger64
{
  uint8_t array[8];
  uint64_t integer;
};

void
espSendCertificateRequestInterest(uint8_t* tokenArray) {

  Serial.println("Sending certificate request interest.");

  ArrayToInteger64 converter;

  int i = 0;
  int j = 7;
  for (; i < 8, j > -1; i++, j--) {
    converter.array[i] = tokenArray[j];
  }

  Serial.print("Token as interpreted by send certifciate request interest function: ");
  print64(converter.integer);
  Serial.println("");

  ndn_NameComponent nameComps[10];
  ndn::InterestLite interest(nameComps, 10, nullptr, 0, nullptr, 0);
  interest.setName(cert_prefix);
  interest.getName().append("cert");
  interest.getName().append(digestChars);
  // should be code here to insert uri encoded certificate bytes

  uECC_make_key(pubkey, pvtkey);

  generateNDNCertificate(pubkey, 64);

  ndn_NameComponent tempNameComps[PREFIX_NCOMPS + 1];
  ndn_NameComponent key_nameComps[PREFIX_NCOMPS + 1];
  ndn::DataLite tempData(tempNameComps, PREFIX_NCOMPS + 1, key_nameComps, PREFIX_NCOMPS + 1);

  size_t signedPortionBeginOffset1, signedPortionEndOffset1;
  ndn_Error error1;
  if ((error1 = ndn::Tlv0_2WireFormatLite::decodeData
                (tempData, ndnCertificateBytes + certificateGenerationInfo[0], certificateGenerationInfo[1], &signedPortionBeginOffset1,
                 &signedPortionEndOffset1))) {
    Serial.println("Error decoding newly generated certificate.");
    Serial.print("Error: ");
    Serial.println(ndn_getErrorString(error1));
    return;
  }

  ndn::EcPublicKey newPubKey(pubkey);

  uint8_t encoding[1000];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t encodingLength, signedPortionBeginOffset, signedPortionEndOffset;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::encodeData
               (tempData, &signedPortionBeginOffset,
                &signedPortionEndOffset, output, &encodingLength))) {
    Serial.println("Error decoding newly generated certificate.");
    Serial.print("Error: ");
    Serial.println(ndn_getErrorString(error));
    return;
  }

  Serial.print("Signed portion begin offset: ");
  Serial.println(signedPortionBeginOffset);
  Serial.print("Signed portion end offset: ");
  Serial.println(signedPortionEndOffset);
  Serial.print("Encoding length: ");
  Serial.println(encodingLength);

  if (newPubKey.verify(encoding + signedPortionBeginOffset, signedPortionEndOffset - signedPortionBeginOffset,
                       tempData.getSignature().getSignature().buf(), tempData.getSignature().getSignature().size())) {
    Serial.println("Successfully verified newly generated certificate.");
  }
  else {
    Serial.println("Failed to verify newly generated certificate.");
    return;
  }

  Serial.println("Key locator name of newly generated certificate:");
  ndn::PrintUri(tempData.getSignature().getKeyLocator().getKeyName());

  Serial.println("Name of newly generated certificate:");
  ndn::PrintUri(tempData.getName());

  Serial.println("Bytes of generated public key:");
  printByteArray(pubkey, 0, 64);

  Serial.println("Bytes of generated private key:");
  printByteArray(pvtkey, 0, 32);

  interest.getName().append(ndnCertificateBytes + certificateGenerationInfo[0], certificateGenerationInfo[1]);

  /* Code to do the token signing */
  const uint8_t* input = converter.array;
  size_t inputLen = 8;
  uint8_t sig[ndn::EcPrivateKey::MAX_SIG_LENGTH];

  int resLength = g_generatedPvtKey.sign(input, inputLen, sig);

  if (resLength == 0) {
    Serial.println("Failed to sign token during certificate request generation.");
    return;
  }

  interest.getName().append(sig, resLength);

  //loadTokenString(converter.integer);

  //interest.getName().append(tokenStringArray);

  // interest.setCanBePrefix(true);
  interest.setMustBeFresh(true);

  g_face.setSigningKey(g_bootstrapPvtKey);
  g_face.sendSignedInterest(interest);
  Serial << "<I " << ndn::PrintUri(interest.getName()) << endl;
}

void
printInterestBytes(ndn::InterestLite interest) {

  ndn_NameComponent interestNameComponents[10];
  struct ndn_ExcludeEntry excludeEntries[9];

  // Encode once to get the signed portion.
  uint8_t encoding[240];
  ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
  size_t encodingLength, signedPortionBeginOffset, signedPortionEndOffset;
  ndn_Error error;
  if ((error = ndn::Tlv0_2WireFormatLite::encodeInterest
               (interest, &signedPortionBeginOffset, &signedPortionEndOffset,
                output, &encodingLength)))
    Serial.println("Failure encoding once to get signed portion.");

  int counter = 0;
  for (int i = 0; i < encodingLength; i++) {
    p(encoding[i]);

    counter++;
    if (counter > 5) {
      counter = 0;
      Serial.println("");
    }

  }
  Serial.println("");

}

void
espProcessData(void*, const ndn::DataLite& data, uint64_t)
{
  const ndn::NameLite& name = data.getName();
  Serial << ">D " << ndn::PrintUri(name) << endl;

  if (name.get(-4).equals(digest_chars_name.get(0))) {
    Serial.println("Processing data from a bootstrapping request.");

    const uint8_t* dataContentBytes = data.getContent().buf();
    int dataContentSize = data.getContent().size();

    int curIndex = 0;
    int currentTLVLength, currentTLVLengthEncodingLength;

    Serial.println("Bytes of content of nested data packet: ");
    printByteArray(dataContentBytes, 0, dataContentSize);

    curIndex = 0;
    if (dataContentBytes[0] != 0x81) {
      Serial.println("First part of data content was not token, ignoring data packet.");
      return;
    }

    if (dataContentBytes[1] != 8) {
      Serial.println("Token was not expected length (8 bytes), ignoring data packet.");
      return;
    }

    for (int i = 2; i < 8 + 2; i++) {
      tokenArray[i - 2] = dataContentBytes[i];
      curIndex = i + 1;
    }

    if (dataContentBytes[curIndex] != 0x06) {
      Serial.println("Second part of data content was not data TLV, ignoring data packet.");
      return;
    }

    // increment curIndex to move to data's TLV length
    curIndex += 1;

    if (dataContentBytes[curIndex] == NULL) {
      Serial.println("Reached end of data content bytes prematurely, ignoring data packet.");
      return;
    }

    currentTLVLength = getTLVLength((uint8_t *) &dataContentBytes[curIndex]);
    currentTLVLengthEncodingLength = getTLVLengthEncodingLength(dataContentBytes[curIndex]);

    Serial.print("TLV encoding length: ");
    Serial.println(currentTLVLengthEncodingLength);
    Serial.print("TLV length: ");
    Serial.println(currentTLVLength);

    curIndex += currentTLVLengthEncodingLength;
    if (currentTLVLengthEncodingLength > 1) {
      curIndex++;
    }

    if (dataContentBytes[curIndex] != 0x07) {
      Serial.println("First part of nested data TLV was not name, ignoring data packet.");
      return;
    }

    // increment curIndex to move to name's TLV length
    curIndex += 1;

    if (dataContentBytes[curIndex] == NULL) {
      Serial.println("Reached end of data content bytes prematurely, ignoring data packet.");
      return;
    }

    currentTLVLength = getTLVLength((uint8_t *) &dataContentBytes[curIndex]);
    currentTLVLengthEncodingLength = getTLVLengthEncodingLength(dataContentBytes[curIndex]);

    Serial.print("TLV encoding length: ");
    Serial.println(currentTLVLengthEncodingLength);
    Serial.print("TLV length: ");
    Serial.println(currentTLVLength);

    curIndex += currentTLVLengthEncodingLength + currentTLVLength;
    if (currentTLVLengthEncodingLength > 1) {
      curIndex++;
    }

    if (dataContentBytes[curIndex] != 0x14) {
      Serial.println("Second part of nested data TLV was not Metainfo, ignoring data packet.");
      return;
    }

    // increment curIndex to get to metainfo's length
    curIndex += 1;

    if (dataContentBytes[curIndex] == NULL) {
      Serial.println("Reached end of data content bytes prematurely, ignoring data packet.");
      return;
    }

    currentTLVLength = getTLVLength((uint8_t *) &dataContentBytes[curIndex]);
    currentTLVLengthEncodingLength = getTLVLengthEncodingLength(dataContentBytes[curIndex]);

    Serial.print("TLV encoding length: ");
    Serial.println(currentTLVLengthEncodingLength);
    Serial.print("TLV length: ");
    Serial.println(currentTLVLength);

    curIndex += currentTLVLengthEncodingLength + currentTLVLength;
    if (currentTLVLengthEncodingLength > 1) {
      curIndex++;
    }

    if (dataContentBytes[curIndex] != 0x15) {
      Serial.println("Third part of nested data TLV was not content, ignoring data packet.");
      return;
    }

    curIndex += 1;

    int controllerPubKeyEncodedLength = getTLVLength((uint8_t *) &dataContentBytes[curIndex]);
    currentTLVLengthEncodingLength = getTLVLengthEncodingLength(dataContentBytes[curIndex]);

    curIndex += currentTLVLengthEncodingLength;
    if (currentTLVLengthEncodingLength > 1) {
      curIndex++;
    }

    Serial.print("Length of content of nested data tlv: ");
    Serial.println(controllerPubKeyEncodedLength);

    uint8_t controllerPubKeyEncodedBytes[controllerPubKeyEncodedLength];
    for (int i = 0; i < controllerPubKeyEncodedLength; i++) {
      controllerPubKeyEncodedBytes[i] = dataContentBytes[i + curIndex];
    }

    // this code to get the nested data packet without parsing on the byte level
    // failed because of the time functions are not supported by the standard library error
    /*
      uint16_t nestedDataLength = dataContentBytes[curIndex + 1];;
      if (nestedDataLength < 253) {
        nestedDataLength += 2;
      }
      else if (nestedDataLength == 0xfd) {
        ArrayToInteger converter;
        converter.array[1] = dataContentBytes[curIndex + 2];
        converter.array[0] = dataContentBytes[curIndex + 3];
        nestedDataLength = converter.integer;
        nestedDataLength += 4;
      }
      else {
        Serial.println("Nested data length was too long, ignoring data packet.");
        return;
      }

      Serial.print("Nested data length: ");
      Serial.println(nestedDataLength);

      ndn_NameComponent nameComps[PREFIX_NCOMPS + 1];
      ndn_NameComponent key_nameComps[PREFIX_NCOMPS + 1];
      ndn::DataLite nestedData(nameComps, PREFIX_NCOMPS + 1, key_nameComps, PREFIX_NCOMPS + 1);

      printByteArray(dataContentBytes, curIndex, curIndex + nestedDataLength);

      size_t signedPortionBeginOffset, signedPortionEndOffset;
      ndn_Error error;
      if ((error = ndn::Tlv0_2WireFormatLite::decodeData
                   (nestedData, dataContentBytes + curIndex, nestedDataLength,&signedPortionBeginOffset,
                    &signedPortionEndOffset))) {
        Serial.println("Error decoding nested data packet in bootstrapping request response.");
        Serial.print("Error: ");
        Serial.println(ndn_getErrorString(error));
        return;
      }

      const uint8_t *controllerPubKeyEncodedBytes = nestedData.getContent().buf();

      Serial.print("Nested data length after decoding: ");
      Serial.println(nestedData.getContent().size());
    */

    // code to get public key from ANS.1 encoding
    /**************************/

    Serial.println("Now attempting to decode public key.");
    int curPubKeyEncodingIndex;
    if (controllerPubKeyEncodedBytes[0] != 0x30 || controllerPubKeyEncodedBytes[2] != 0x30) {
      Serial.println("Did not get the first two expected types for public key's encoding, exiting.");
      return;
    }
    curPubKeyEncodingIndex = 4;
    curPubKeyEncodingIndex += controllerPubKeyEncodedBytes[3];
    Serial.print("Value of encoded bytes at index 3: ");
    Serial.println(controllerPubKeyEncodedBytes[3]);
    Serial.print("value of encoded bytes at index after adding index 3: ");
    Serial.println(controllerPubKeyEncodedBytes[curPubKeyEncodingIndex]);
    if (controllerPubKeyEncodedBytes[curPubKeyEncodingIndex] != 0x03) {
      Serial.println("Did not get expected type for public key's actual bytes, exiting.");
      return;
    }
    curPubKeyEncodingIndex += 1;
    int controllerPubKeyLength = controllerPubKeyEncodedBytes[curPubKeyEncodingIndex] - 2;
    controller_trust_anchor_length = controllerPubKeyLength;
    curPubKeyEncodingIndex += 1;
    for (int i = 0; i < controllerPubKeyLength; i++) {
      controller_trust_anchor[i] = controllerPubKeyEncodedBytes[curPubKeyEncodingIndex + i + 2];
    }

    Serial.println("Bytes of encoded public key:");
    printByteArray(controllerPubKeyEncodedBytes, 0, controllerPubKeyEncodedLength);

    Serial.println("Bytes of public key:");
    printByteArray(controller_trust_anchor, 0, controllerPubKeyLength);

    /***********************************/

    ndn_NameComponent nameComps[PREFIX_NCOMPS + 1];
    ndn_NameComponent key_nameComps[PREFIX_NCOMPS + 1];
    ndn::DataLite tempData(nameComps, PREFIX_NCOMPS + 1, key_nameComps, PREFIX_NCOMPS + 1);

    tempData.set(data);

    ndn::EcPublicKey controllerPubKey(controller_trust_anchor);

    uint8_t encoding[1000];
    ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
    size_t encodingLength, signedPortionBeginOffset, signedPortionEndOffset;
    ndn_Error error;
    if ((error = ndn::Tlv0_2WireFormatLite::encodeData
                 (tempData, &signedPortionBeginOffset,
                  &signedPortionEndOffset, output, &encodingLength))) {
      Serial.println("Error decoding data packet for certificate request response.");
      Serial.print("Error: ");
      Serial.println(ndn_getErrorString(error));
      return;
    }

    Serial.print("Signed portion begin offset: ");
    Serial.println(signedPortionBeginOffset);
    Serial.print("Signed portion end offset: ");
    Serial.println(signedPortionEndOffset);
    Serial.print("Encoding length: ");
    Serial.println(encodingLength);

    if (controllerPubKey.verify(encoding + signedPortionBeginOffset, signedPortionEndOffset - signedPortionBeginOffset,
                                tempData.getSignature().getSignature().buf(), tempData.getSignature().getSignature().size())) {
      Serial.println("Successfully verified bootstrapping request response data packet.");
    }
    else {
      Serial.println("Bootstrapping request response data packet failed to verify, ignoring it.");
      return;
    }

    espSendCertificateRequestInterest(tokenArray);
  }
  else if (name.get(0).equals(cert_prefix.get(0))) {
    Serial.println("Got a reply to certificate request.");

    ndn_NameComponent nameComps[PREFIX_NCOMPS + 1];
    ndn_NameComponent key_nameComps[PREFIX_NCOMPS + 1];
    ndn::DataLite tempData(nameComps, PREFIX_NCOMPS + 1, key_nameComps, PREFIX_NCOMPS + 1);

    tempData.set(data);

    ndn::EcPublicKey controllerPubKey(controller_trust_anchor);

    uint8_t encoding[1000];
    ndn::DynamicUInt8ArrayLite output(encoding, sizeof(encoding), 0);
    size_t signedPortionBeginOffset, signedPortionEndOffset, encodingLength;
    ndn_Error error;
    if ((error = ndn::Tlv0_2WireFormatLite::encodeData
                 (tempData, &signedPortionBeginOffset,
                  &signedPortionEndOffset, output, &encodingLength))) {
      Serial.println("Error encoding data packet for certificate request response.");
      Serial.print("Error: ");
      Serial.println(ndn_getErrorString(error));
      return;
    }

    Serial.print("Signed portion begin offset: ");
    Serial.println(signedPortionBeginOffset);
    Serial.print("Signed portion end offset: ");
    Serial.println(signedPortionEndOffset);
    Serial.print("Encoding length: ");
    Serial.println(encodingLength);

    if (controllerPubKey.verify(encoding + signedPortionBeginOffset, signedPortionEndOffset - signedPortionBeginOffset,
                                tempData.getSignature().getSignature().buf(), tempData.getSignature().getSignature().size())) {
      Serial.println("Successfully verified certificate request response data packet.");
    }
    else {
      Serial.println("Certificate request response data packet failed to verify, ignoring it.");
      return;
    }

    ndn::DataLite receivedCertificate(nameComps, PREFIX_NCOMPS + 1, key_nameComps, PREFIX_NCOMPS + 1);

    received_certificate_size = data.getContent().size();
    memcpy(received_certificate_bytes, data.getContent().buf(), received_certificate_size);

    if ((error = ndn::Tlv0_2WireFormatLite::decodeData
                 (receivedCertificate, received_certificate_bytes, received_certificate_size, &signedPortionBeginOffset,
                  &signedPortionEndOffset))) {
      Serial.println("Error decoding received certificate.");
      Serial.print("Error: ");
      Serial.println(ndn_getErrorString(error));
      return;
    }

    Serial.print("Key locator of received certificate: ");
    Serial << ndn::PrintUri(receivedCertificate.getSignature().getKeyLocator().getKeyName());
    Serial.println("");

    if ((error = ndn::Tlv0_2WireFormatLite::encodeData
                 (receivedCertificate, &signedPortionBeginOffset,
                  &signedPortionEndOffset, output, &encodingLength))) {
      Serial.println("Error encoding received certificate.");
      Serial.print("Error: ");
      Serial.println(ndn_getErrorString(error));
      return;
    }

    Serial.print("Signed portion begin offset: ");
    Serial.println(signedPortionBeginOffset);
    Serial.print("Signed portion end offset: ");
    Serial.println(signedPortionEndOffset);
    Serial.print("Encoding length: ");
    Serial.println(encodingLength);

    if (controllerPubKey.verify(encoding + signedPortionBeginOffset, signedPortionEndOffset - signedPortionBeginOffset,
                                receivedCertificate.getSignature().getSignature().buf(), receivedCertificate.getSignature().getSignature().size())) {
      Serial.println("Successfully verified received certificate.");
    }
    else {
      Serial.println("Received certificate failed to verify, ignoring it.");
      return;
    }

    g_face.setSigningKey(g_generatedPvtKey);

    Serial.println("Successfully verified the network, accepting interests for sensor data.");
    is_onboarded = true;

    u8g2.clearBuffer();
    u8g2.drawStr(0, 16, "Successfully completed \n onboarding.");
    u8g2.sendBuffer();

  }
}

bool
espReplyNack(const ndn::InterestLite& interest, ndn_NetworkNackReason nackReason)
{
  ndn::NetworkNackLite nack;
  nack.setReason(nackReason);
  g_face.sendNack(nack, interest);
  Serial << "<N " << ndn::PrintUri(interest.getName()) << '~' << nackReason << endl;
  return true;
}

bool
espPerformReading(const ndn::InterestLite& interest, int readType)
{
  uint64_t timestamp = millis();
  if (!g_bme.performReading()) {
    Serial.println("g_bme reading failed.");
    return espReplyNack(interest, ndn_NetworkNackReason_CONGESTION);
  }
  Serial << "Performing reading at " << static_cast<unsigned long>(timestamp) << endl;

  char contentBuf[PAYLOAD_BUFSIZE];
  PString content(contentBuf, sizeof(contentBuf));
  switch (readType) {
    case 0:
      content << g_bme.temperature << "\n" << "~";
      break;
    case 1:
      content << g_bme.humidity << "\n" << "~";
      break;
    case 2:
      content << g_bme.pressure << "\n" << "~";
      break;
    case 3:
      content << g_bme.gas_resistance << "\n" << "~";
      break;
    case 4:
      content << currentOccupancy << "\n" << "~";
      break;
    default:
      break;
  };

  Serial.println("Content in data packet : ");
  Serial.println(content);

  ndn_NameComponent nameComps[PREFIX_NCOMPS + 1];
  ndn_NameComponent keyNameComps[PREFIX_NCOMPS + 1];
  ndn::DataLite data(nameComps, PREFIX_NCOMPS + 1, keyNameComps, PREFIX_NCOMPS + 1);

  ndn::NameLite& name = data.getName();
  name.set(interest.getName());
  uint8_t timestampBuf[9];
  //name.appendTimestamp(timestamp, timestampBuf, sizeof(timestampBuf));

  data.getMetaInfo().setFreshnessPeriod(1);

  data.setContent(ndn::BlobLite(reinterpret_cast<const uint8_t*>(contentBuf), content.length()));

  g_face.setSigningKey(g_generatedPvtKey);

  g_face.sendData(data);
  Serial << " < D " << ndn::PrintUri(name) << endl;

  u8g2.clearBuffer();
  u8g2.setCursor(0, 8);
  u8g2 << static_cast<unsigned long>(timestamp);
  u8g2.sendBuffer();

  return true;
}

void
espProcessInterest(void*, const ndn::InterestLite& interest, uint64_t)
{

  if (!is_onboarded) {

    Serial.println("Not yet onboarded, ignoring sensor data interest.");
    return;

  }

  /*
    if (g_command_prefix.equals(interest.getName())) {
    Serial.println("Got a direct interest, toggling LED.");

    digitalWrite(PIN_LED, !digitalRead(PIN_LED));
    }
    else {
    Serial << " < I " << ndn::PrintUri(interest.getName()) << endl;
    espPerformReading(interest) ||
    espReplyNack(interest, ndn_NetworkNackReason_NO_ROUTE);
    }
  */
  Serial << "Got interest: " << ndn::PrintUri(interest.getName()) << "\n";

  if (interest.getName().get(-3).equals(temperature_name.get(0))) {
    Serial.println("Got interest for temperature data");
    espPerformReading(interest, 0);
  }
  else if (interest.getName().get(-3).equals(humidity_name.get(0))) {
    Serial.println("Got interest for humidity data");
    espPerformReading(interest, 1);
  }
  else if (interest.getName().get(-3).equals(airPressure_name.get(0))) {
    Serial.println("Got interest for air pressure data");
    espPerformReading(interest, 2);
  }
  else if (interest.getName().get(-3).equals(gasResistance_name.get(0))) {
    Serial.println("Got interest for gas resistance data");
    espPerformReading(interest, 3);
  }
  else if (interest.getName().get(-3).equals(occupancy_name.get(0))) {
    Serial.println("Got interest for current building occupancy");
    espPerformReading(interest, 4);
  }
}

void
setupESP()
{
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  Serial << "ESP32 BME680 NDN demo - consumer side" << endl;
  WiFi.config(IP_CLIENT, IP_SERVER, IP_SUBNET);
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  while (WiFi.status() != WL_CONNECTED)
    ;
  Serial << "Connected to access point." << endl;
  Serial << "IP address : " << WiFi.localIP() << endl;
  digitalWrite(PIN_LED, HIGH);

  g_transport.begin(IP_SERVER, UDP_PORT, UDP_PORT);
  g_face.setSigningKey(g_bootstrapPvtKey);
  g_face.onInterest(&espProcessInterest, nullptr);
  g_face.onData(&espProcessData, nullptr);

  if (!g_bme.begin()) {
    Serial << "BME680 not found" << endl;
    ESP.restart();
  }
  g_bme.setTemperatureOversampling(BME680_OS_8X);
  g_bme.setHumidityOversampling(BME680_OS_2X);
  g_bme.setPressureOversampling(BME680_OS_4X);
  g_bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  g_bme.setGasHeater(320, 150);
  Serial << "Initialized BME680 sensor" << endl;

  uint8_t versionBuffer[20];
  cert_name.append(deviceID);
  cert_name.append("KEY");
  cert_name.append("key-id");
  cert_name.append("self");
  cert_name.appendVersion((unsigned long long) millis(), versionBuffer, 20);

  u8g2.clearBuffer();
  u8g2.drawStr(0, 16, WiFi.localIP().toString().c_str());
  u8g2.sendBuffer();
}

void
setup()
{
  Serial.begin(115200);
  Serial.println();

  ndn::CryptoLite::digestSha256(BOOTSTRAP_ECC_PUBLIC, 91, ECC_PUBLIC_DIGEST);

  for (int i = 0; i < 64; i += 2) {
    putInCharBuf(digestChars, i, ECC_PUBLIC_DIGEST[i / 2]);
  }
  digestChars[65] = '\0';

  Serial.println(digestChars);

  Serial.println("");

  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_BTN, INPUT);
  pinMode(PIN_PIR1, INPUT);
  pinMode(PIN_PIR2, INPUT);

  u8g2.begin();
  u8g2.setFont(u8g2_font_helvR08_tf);

  ndn::setLogOutput(Serial);
  ndn::parseNameFromUri(g_command_prefix, COMMAND_PREFIX);
  ndn::parseNameFromUri(bootstrap_prefix, BOOTSTRAP_PREFIX);
  ndn::parseNameFromUri(cert_prefix, CERT_PREFIX);
  ndn::parseNameFromUri(digest_chars_name, digestChars);
  ndn::parseNameFromUri(temperature_name, temperatureString);
  ndn::parseNameFromUri(humidity_name, humidityString);
  ndn::parseNameFromUri(airPressure_name, airPressureString);
  ndn::parseNameFromUri(gasResistance_name, gasResistanceString);
  ndn::parseNameFromUri(occupancy_name, occupancyString);

  setupESP();
}

void
loopNotOnboarded()
{
  static bool wasBtnDown = false;
  bool isBtnDown = !digitalRead(PIN_BTN);
  if (!wasBtnDown && isBtnDown) {
    espSendBootstrappingInterest();
  }
  wasBtnDown = isBtnDown;
}

void
loopOnboarded()
{

}

int lastModePIR1 = 0;
int lastModePIR2 = 0;

int motionWaitTime = 3000;
int motionStartTimer = 0;

bool checkForBuildingEntry = false;
bool checkForBuildingExit = false;

// returns 0 if no change from last state, 1 if motion detected, -1 if motion stopped
int
motionCheck(int pirPin, int &lastMode) {
  if (digitalRead(pirPin) == HIGH && lastMode == 0) {
    lastMode = 1;
    return 1;
  }
  else if (digitalRead(pirPin) == LOW && lastMode == 1) {
    lastMode = 0;
    return -1;
  }

  return 0;
}

void
pirSensingLoop() {
  int currentMotionPIR1 = motionCheck(PIN_PIR1, lastModePIR1);
  int currentMotionPIR2 = motionCheck(PIN_PIR2, lastModePIR2);

  if (currentMotionPIR1 == 1) {
    Serial.println("Motion started for pir 1.");
    if (!checkForBuildingEntry && !checkForBuildingExit)
      checkForBuildingEntry = true;
  }
  else if (currentMotionPIR1 == -1) {
    Serial.println("Motion stopped for pir 1.");
  }

  if (currentMotionPIR2 == 1) {
    Serial.println("Motion started for pir 2.");
    if (!checkForBuildingEntry && !checkForBuildingExit)
      checkForBuildingExit = true;
  }
  else if (currentMotionPIR2 == -1) {
    Serial.println("Motion stopped for pir 2.");
  }

  if (checkForBuildingEntry) {
    if (motionStartTimer == 0) {
      motionStartTimer = millis();
    }
    else if (millis() < motionStartTimer + motionWaitTime) {
      if (currentMotionPIR2 == 1) {
        Serial.println("Someone entered the building.");
        motionStartTimer = 0;
        currentOccupancy++;
        Serial.print("Current building occupancy: ");
        Serial.println(currentOccupancy);
        checkForBuildingEntry = false;
      }
    }
    else {
      Serial.println("Motion wait time ran out, ignoring building entry starting motion.");
      motionStartTimer = 0;
      checkForBuildingEntry = false;
    }
  }

  if (checkForBuildingExit) {
    if (motionStartTimer == 0) {
      motionStartTimer = millis();
    }
    else if (millis() < motionStartTimer + motionWaitTime) {
      if (currentMotionPIR1 == 1) {
        Serial.println("Someone exited the building.");
        motionStartTimer = 0;
        currentOccupancy--;
        Serial.print("Current building occupancy: ");
        Serial.println(currentOccupancy);
        checkForBuildingExit = false;
      }
    }
    else {
      Serial.println("Motion wait time ran out, ignoring building exit starting motion.");
      motionStartTimer = 0;
      checkForBuildingExit = false;
    }
  }
}


void
loop()
{

  if (WiFi.status() != WL_CONNECTED) {
    Serial << "WiFi disconnected" << endl;
    ESP.restart();
  }


  is_onboarded ? loopOnboarded() : loopNotOnboarded();
  g_face.loop();

  pirSensingLoop();

}

