import sys

from pyndn import Name, Data, HmacWithSha256Signature, KeyLocatorType
from pyndn.security import KeyChain
from pyndn.util import Blob
from pyndn import Face, Interest
import time, random
from threading import Thread
from pyndn.encoding import ProtobufTlv
from datetime import datetime
import signal
from binascii import unhexlify

from timestamp_fetcher import TimestampFetcher

from cbor2.decoder import loads
from cbor2.compat import timezone
from cbor2.encoder import dumps, CBOREncodeError, shareable_encoder
from cbor2.types import CBORTag, undefined, CBORSimpleValue, FrozenDict

def dump(*list):
    result = ""
    for element in list:
        result += (element if type(element) is str else str(element)) + " "
    print(result)

def handler(signal, frame):
    print('Caught keyboard interrupt, exiting...\n')
    global t
    global t2
    t = None
    t2 = None
    face.shutdown();
    with open('%s' % (deviceIDListName), 'a') as the_file:
        the_file.seek(0)
        the_file.truncate(0)
    exit()

def run_face_processing():
    while True:
        face.processEvents()
        # We need to sleep for a few milliseconds so we don't use 100% of the CPU.
        time.sleep(0.01)

def onComplete(retrievedBlobs, originalRequestName):

    read_type = originalRequestName.get(-2).toNumber()

    print "Read type: " + str(read_type)
    
    num_intervals = originalRequestName.get(-6).toNumber()

    print "Number of intervals of completed request: " + str(num_intervals)

    global currentOutstandingInterests
    currentOutstandingInterests -= (num_intervals + 1)

    print "Current outstanding interests: " + str(currentOutstandingInterests)

    print "Finished fetching blobs for: " + originalRequestName.toUri()
    print "Number of retrieved blobs: " + str(len(retrievedBlobs))

    aggregationType = str(originalRequestName.get(-7).getValue())
    print "Aggregation type: " + aggregationType
    
    i = 0
    while i < len(retrievedBlobs):
            print "retrievedBlobs[" + str(i) + "]: " + str(retrievedBlobs[i])
            i += 1

    interpretedBlobs = convertDataPacketBlobsToSensorValueBlobs(retrievedBlobs, read_type) 
            
    answer = 0
    if (aggregationType == "avg"):
        answer = calculateAvg(interpretedBlobs)
    elif (aggregationType == "sum"):
        answer = calculateSum(interpretedBlobs)
    elif (aggregationType == "min"):
        answer = calculateMin(interpretedBlobs)
    elif (aggregationType == "max"):
        answer = calculateMax(interpretedBlobs)

    # Make and sign a Data packet.
    data = Data(originalRequestName)

    obj = [answer]
    serialized = dumps(obj)
        
    content = serialized
    data.setContent(content)
    keyChain.sign(data, keyChain.getDefaultCertificateName())
    
    dump("Sent content", content)
    face.putData(data)

def convertDataPacketBlobsToSensorValueBlobs(dataPacketBlobs, originalSensorType):
    print "Original sensor type in converting data packet blobs to sensor value blobs " + str(originalSensorType)
    repackagedSensorValues = []

    j = 0
    while j < len(dataPacketBlobs):
        repackagedSensorValues.append(interpretDataWithCBOREncodedContent(dataPacketBlobs[j], originalSensorType))
        j += 1

    return repackagedSensorValues

def interpretDataWithCBOREncodedContent(dataPacketBlob, originalSensorType):

    print "Interpreting data with cbor encoded content, original sensor type: " + str(originalSensorType)
    
    dataPacket = Data()
    dataPacket.wireDecode(dataPacketBlob)
    
    nameOfDataPacket = dataPacket.getName()
    print "Name of data packet with cbor encoded content: " + nameOfDataPacket.toUri()

    sensorTypesBytes = nameOfDataPacket.get(-1).getValue().buf()
    
    sensorTypesInDataPacketName = []

    i = 0
    while i+1 < len(sensorTypesBytes):
        currentSensorType = sensorTypesBytes[i]<<8 | sensorTypesBytes[i+1]
        print "Current sensor type: " + str(currentSensorType)
        sensorTypesInDataPacketName.append(str(currentSensorType))
        i += 2

    # okay so here: how do i convert cborEncodedContent into sensorValuesInCboorEncodedContent??
    cborEncodedContent = dataPacket.getContent()
    bytesOfCborEncodedContent = cborEncodedContent.toBuffer().tobytes()
    #byteString = bytesOfCborEncodedContent.decode('utf-8')
        
    sensorValuesInCborEncodedContent = loads(bytesOfCborEncodedContent)

    i = 0
    while i < len(sensorValuesInCborEncodedContent):
        print "Index " + str(i) + " of sensor values in cbor encoded content."
        print "Value: " + str(sensorValuesInCborEncodedContent[i])
        i += 1
    
    # okay so now what i have to do is, decode the actual content of the data packet to get
    # a list of values from the cbor payload; then, i can iterate through the sensortypesindatapacket
    # name list until i get to the index of originalSensorType; then i use that same index on the
    # list of content from the cbor encoded data packet to get the corresponding data type from the
    # cbor thing

    i = 0
    while i < len(sensorTypesInDataPacketName):
        if (str(sensorTypesInDataPacketName[i]) == str(originalSensorType)):
            print "Found the corresponding sensor type to " + str(originalSensorType) + " at index " + str(i)
            break
        i += 1

    print "Returning sensor value at index " + str(i)
    blob = Blob(sensorValuesInCborEncodedContent[i])
        
    return blob;
    
def calculateAvg(retrievedBlobs):
    print "Doing an average calculation on the retrieved blobs."
    
    answer = calculateSum(retrievedBlobs)
    
    answer /= len(retrievedBlobs)
    print "Average: " + str(answer)

    return answer

def calculateSum(retrievedBlobs):
    print "Doing a sum calculation on the retrieved blobs."
    j = 0
    answer = 0
    while j < len(retrievedBlobs):
        answer += float(str(retrievedBlobs[j]))
        j += 1
    print "Total: " + str(answer)
    
    return answer
    
def calculateMin(retrievedBlobs):
    print "Doing a min calculation on the retrieved blobs."
    
    j = 0
    answer = float(str(retrievedBlobs[0]))
    while j < len(retrievedBlobs):
        if (float(str(retrievedBlobs[j])) < answer):
            answer = float(str(retrievedBlobs[j]))
        j += 1
    print "Minimum: " + str(answer)
    
    return answer
    
def calculateMax(retrievedBlobs):
    print "Doing a max calculation on the retrieved blobs."
    
    j = 0
    answer = float(str(retrievedBlobs[0]))
    while j < len(retrievedBlobs):
        if (float(str(retrievedBlobs[j])) > answer):
            answer = float(str(retrievedBlobs[j]))
        j += 1
    print "Maximum: " + str(answer)
    
    return answer
    
def onError(originalRequestName, missingTimestamps):
    print "Error retrieving blobs for: " + originalRequestName.toUri()

    num_intervals = originalRequestName.get(-6).toNumber()

    print "Number of intervals of error request: " + str(num_intervals)

    global currentOutstandingInterests
    currentOutstandingInterests -= (num_intervals + 1)

    print "Current outstanding interests: " + str(currentOutstandingInterests)
    
    print "Missing timestamps:"
    errorMessage = ""
    i = 0
    while i < len(missingTimestamps):
        currentLine = "Missing timestamp " + str(i) + ": " + str(missingTimestamps[i][0]) + "\n" + "Error code: " + str(missingTimestamps[i][1]) + "\n"
        print currentLine
        errorMessage += currentLine
        i += 1

    publish_error_data("Some data packets nacked or timed out", originalRequestName)
    
def verifySegment(data):
    print "Verify segment got called."
    return True
    
class AggregationRequestListener(object):
    def __init__(self, keyChain, certificateName):
        self._keyChain = keyChain
        self._certificateName = certificateName

    def onInterest(self, prefix, interest, face, interestFilterId, filter):

        # format of name for aggregation request should follow this format:
        # /<pubsub-prefix>/aggregator/<aggregation type>/<duration>/<sample interval>/<repo-name>/<dev-name>/<read-type>/<timestamp>

        if (interest.getName().size() < 9):
            publish_error_data("Aggregation interest name was too short", interest.getName())
            return
            
        try:
            initial_timestamp = interest.getName().get(-1).toNumber()
            read_type = interest.getName().get(-2).toNumber()
            device_name = str(interest.getName().get(-3).getValue())
            repo_name = str(interest.getName().get(-4).getValue())
            sample_interval = interest.getName().get(-5).toNumber()
            num_intervals = interest.getName().get(-6).toNumber()
            aggregation_type = str(interest.getName().get(-7).getValue())
        except:
            publish_error_data("Error interpreting one of the components from the aggregation interest name", interest.getName())
            return
            
        print "Got aggregation request."
        print "timestamp: " + str(initial_timestamp)
        print "read_type: " + str(read_type)
        print "device_name: " + device_name
        print "repo_name: " + repo_name
        print "sample_interval: " + str(sample_interval)
        print "num_intervals: " + str(num_intervals)
        print "aggregation_type: " + aggregation_type
        print "pubsubPrefix: " + pubsubPrefix

        if (not checkAggregationType(aggregation_type)):
            publish_error_data("Invalid aggregation type " + aggregation_type, interest.getName())
            return

        if (not checkReadType(read_type)):
            publish_error_data("Invalid read type " + str(read_type), interest.getName())
            return
        
        if (not timeRangeIsValid(sample_interval, num_intervals, initial_timestamp, read_type, device_name)):
            publish_error_data("Invalid time range for request", interest.getName())
            return

        if (not numIntervalsIsValid(num_intervals)):
            publish_error_data("Invalid number of intervals, max is: " + str(maxOutstandingInterests-1), interest.getName())
            return
        
        while (not belowMaxOutstandingInterests(num_intervals)):
            print "Waiting for current outstanding interest number to reduce before sending aggregation request..."

        print "We are now below the max outstanding interest limit, sending aggregation request."
        global currentOutstandingInterests
        currentOutstandingInterests += num_intervals + 1
        print "Current outstanding interests: " + str(currentOutstandingInterests)
        print "Maximum outstanding interests: " + str(maxOutstandingInterests)
        
        timestampFetcher = TimestampFetcher(face, None, verifySegment, onComplete, onError, initial_timestamp, num_intervals, sample_interval, interest.getName())

        initialInterest = Interest(Name(pubsubPrefix).append(repo_name).append(device_name).append(Name.Component.fromNumber(read_type)))

        print "Name of initial interest that will be passed to timestamp fetcher: " + initialInterest.getName().toUri()
        
        timestampFetcher.fetch(face, initialInterest, None, onComplete, onError, initial_timestamp, num_intervals, sample_interval, interest.getName())

    def onRegisterFailed(self, prefix):
        self._responseCount += 1
        dump("Register failed for prefix", prefix.toUri())

def checkAggregationType(aggregation_type):

    if (aggregation_type != "avg" and aggregation_type != "sum" and aggregation_type != "min" and aggregation_type != "max"):
        return False
    
    return True

def checkReadType(read_type):

    if (read_type != 4097 and read_type != 4098 and read_type != 4099 and read_type != 4100 and read_type):
        return False

    return True

def timeRangeIsValid(sample_interval, num_intervals, initial_timestamp, read_type, device_name):

    if initial_timestamp < 0:
        return False
    
    latestTimeRequested = initial_timestamp + (sample_interval * num_intervals)
    print "Latest time requested: " + str(latestTimeRequested)

    fname = "/home/pi/repo-ng/seq/" + device_name + ".seq"
    with open(fname) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]

    i = 0
    while i < len(content):
        words = content[i].split()
        j = 0
        currentType = long(words[0])
        latestTime = int(words[2])
        if currentType == read_type:
            print "Found latest time " + str(latestTime) + " for type " + str(currentType)
            if latestTime >= latestTimeRequested:
                print "Time range of request was valid"
                return True
            else:
                print "Time range of request was invalid."
                print "Time stamp requested: " + str(latestTimeRequested)
        i+= 1

    return False

def numIntervalsIsValid(num_intervals):

    if ((num_intervals + 1) > maxOutstandingInterests):
        return False

    return True

def belowMaxOutstandingInterests(num_intervals):
    if ((num_intervals + 1) + currentOutstandingInterests > maxOutstandingInterests):
        return False
    return True

def publish_error_data(errorMessage, interestName):
    
    print errorMessage + ": " + interestName.toUri()
    
    # Make and sign a Data packet.
    data = Data(interestName)

    obj = [str(errorMessage)]
    serialized = dumps(obj)
    
    content = serialized
    data.setContent(content)
    keyChain.sign(data, keyChain.getDefaultCertificateName())
    
    dump("Sent content", content)
    face.putData(data)
    
        
def listenForAggregationRequests():
    # Also use the default certificate name to sign data packets.
    aggregationRequestListener = AggregationRequestListener(keyChain, keyChain.getDefaultCertificateName())
    prefix = Name(pubsubPrefix).append("aggregator")
    dump("Register prefix", prefix.toUri())
    face.registerPrefix(prefix, aggregationRequestListener.onInterest, aggregationRequestListener.onRegisterFailed)


if len(sys.argv) < 2:
    print "Usage:"
    print "python aggregator.py <pubsub-prefix> <max-outstanding-interests>"
        
global face
# The default Face will connect using a Unix socket, or to "localhost".
face = Face()

global pubsubPrefix
pubsubPrefix = str(sys.argv[1])

global maxOutstandingInterests
maxOutstandingInterests = int(sys.argv[2])

global currentOutstandingInterests
currentOutstandingInterests = 0

global keychain
# Use the system default key chain and certificate name to sign commands.
keyChain = KeyChain()
face.setCommandSigningInfo(keyChain, keyChain.getDefaultCertificateName())

listenForAggregationRequests()

t = Thread(target=run_face_processing)
t.daemon = True
t.start()

signal.signal(signal.SIGINT, handler)

while True:
    t.join(600)
    if not t.isAlive():
        break

face.shutdown()
