
import time
import sys
from pyndn import Name
from pyndn import Face
from pyndn import Interest

from cbor2.decoder import loads

def dump(*list):
    result = ""
    for element in list:
        result += (element if type(element) is str else str(element)) + " "
    print(result)

class DataFetcher(object):
    def __init__(self):
        self._callbackCount = 0
        
    def onData(self, interest, data):
        dump("Got data packet with name", data.getName().toUri())
        # Use join to convert each byte to chr.

        sensorValuesInCborEncodedContent = loads(str(data.getContent()))

        i = 0
        while i < len(sensorValuesInCborEncodedContent):
            print "Index " + str(i) + " of sensor values in cbor encoded content."
            print "Value: " + str(sensorValuesInCborEncodedContent[i])
            i += 1

        print "Content: " + str(sensorValuesInCborEncodedContent[0])
        
        self._callbackCount += 1

    def onTimeout(self, interest):
        dump("Time out for interest", interest.getName().toUri())
        self._callbackCount += 1
        
def main():
    face = Face("localhost")

    if len(sys.argv) < 5:
        print "Usage:"
        print "python tester.py <read-type> <aggregation-type> <sample_interval> <num_intervals> <initial_timestamp>"
        return

    read_type = long(sys.argv[1])
    aggregation_type = str(sys.argv[2])
    sample_interval = int(sys.argv[3])
    num_intervals = int(sys.argv[4])
    initial_timestamp = int(sys.argv[5])
    
    # Try to fetch anything.
    #name = Name("/NIST/library/mainroom/aggregator/avg").append(Name.Component.fromNumber(3)).append(Name.Component.fromNumber(5)).append("repo1").append("ESP32ID001").append("temperature").append(Name.Component.fromNumber(5))
    name = Name("/NIST/library/mainroom/aggregator/" + aggregation_type).append(Name.Component.fromNumber(num_intervals)).append(Name.Component.fromNumber(sample_interval)).append("repo1").append("ESP32ID001").append(Name.Component.fromNumber(read_type)).append(Name.Component.fromNumber(initial_timestamp))
    dataFetcher = DataFetcher()
    interest = Interest(name)
    interest.setInterestLifetimeMilliseconds(1000)
    interest.setMustBeFresh(False)
    print "Expressing name " + name.toUri()
    face.expressInterest(name, dataFetcher.onData, dataFetcher.onTimeout)
    
    while dataFetcher._callbackCount < 1:

        """
        if (str(sys.stdin.readline()) == "s\n"):
            # Try to fetch anything.
            name = Name("/NIST/library/mainroom/aggregator/avg").append(Name.Component.fromNumber(5)).append(Name.Component.fromNumber(50)).append("repo1").append("ESP32ID001").append("temperature").append(Name.Component.fromNumber(0))
            dataFetcher = DataFetcher()
            interest = Interest(name)
            interest.setInterestLifetimeMilliseconds(1000)
            print "Expressing name " + name.toUri()
            face.expressInterest(name, dataFetcher.onData, dataFetcher.onTimeout)
        """
        
        face.processEvents()
        # We need to sleep for a few milliseconds so we don't use 100% of the CPU.
        time.sleep(0.01)

    face.shutdown()
 
main()
