
import logging
from pyndn.interest import Interest
from pyndn.util.blob import Blob
from pyndn import Name

class TimestampFetcher(object):
    """
    A private constructor to create a new SegmentFetcher to use the Face. An
    application should use SegmentFetcher.fetch. If validatorKeyChain is not
    None, use it and ignore verifySegment. After creating the SegmentFetcher,
    call fetchFirstSegment.

    :param Face face: This calls face.expressInterest to fetch more segments.
    :param KeyChain validatorKeyChain: If this is not None, use its verifyData
      instead of the verifySegment callback.
    :param verifySegment: When a Data packet is received this calls
      verifySegment(data) where data is a Data object. If it returns False then
      abort fetching and call onError with
      SegmentFetcher.ErrorCode.SEGMENT_VERIFICATION_FAILED.
    :type verifySegment: function object
    :param onComplete: When all segments are received, call
      onComplete(content) where content is a Blob which has the concatenation of
      the content of all the segments.
      NOTE: The library will log any exceptions raised by this callback, but
      for better error handling the callback should catch and properly
      handle any exceptions.
    :type onComplete: function object
    :param onError: Call onError.onError(errorCode, message) for timeout or an
      error processing segments. errorCode is a value from
      SegmentFetcher.ErrorCode and message is a related string.
      NOTE: The library will log any exceptions raised by this callback, but
      for better error handling the callback should catch and properly
      handle any exceptions.
    :type onError: function object
    """
    def __init__(self, face, validatorKeyChain, verifySegment, onComplete, onError,
                 initialTimestamp, numIntervals, sampleInterval, originalRequestName):
        self._face = face
        self._validatorKeyChain = validatorKeyChain
        self._verifySegment = verifySegment
        self._onComplete = onComplete
        self._onError = onError
        self._initialTimestamp = initialTimestamp
        self._numIntervals = numIntervals
        self._sampleInterval = sampleInterval
        self._originalRequestName = originalRequestName
        self._finalTimestamp = (self._initialTimestamp + (self._numIntervals * self._sampleInterval))

        print "Initialized timestamp fetcher with following parameters: "
        print "Initial timestamp: " + str(self._initialTimestamp)
        print "Number of intervals: " + str(self._numIntervals)
        print "Sampling interval: " + str(self._sampleInterval)
        print "Original request name: " + self._originalRequestName.toUri()
        
        self._retrievedBlobs = [] # of Blobs
        self._missingTimestamps = []
        
    class ErrorCode(object):
        """
        An ErrorCode value is passed in the onError callback.
        """
        INTEREST_TIMEOUT = 1
        DATA_HAS_NO_TIMESTAMP = 2
        DATA_VERIFICATION_FAILED =  3
        INTEREST_NACK = 4

    @staticmethod
    def DontVerifySegment(data):
        """
        DontVerifySegment may be used in fetch to skip validation of Data
        packets.
        """
        return True

    @staticmethod
    def fetch(face, baseInterest, validatorKeyChainOrVerifySegment, onComplete,
              onError, initialTimestamp, numIntervals, sampleInterval, originalRequestName):
        """
        Initiate segment fetching. For more details, see the documentation for
        the module. There are two forms of fetch:
        fetch(face, baseInterest, validatorKeyChain, onComplete, onError)
        and
        fetch(face, baseInterest, verifySegment, onComplete, onError)

        :param Face face: This calls face.expressInterest to fetch more segments.
        :param Interest baseInterest: An Interest for the initial segment of the
          requested data, where baseInterest.getName() has the name prefix.
          This interest may include a custom InterestLifetime and selectors that
          will propagate to all subsequent Interests. The only exception is that
          the initial Interest will be forced to include selectors
          "ChildSelector=1" and "MustBeFresh=true" which will be turned off in
          subsequent Interests.
        :param KeyChain validatorKeyChain: When a Data packet is received this
          calls validatorKeyChain.verifyData(data). If validation fails then
          abort fetching and call onError with SEGMENT_VERIFICATION_FAILED. This
          does not make a copy of the KeyChain; the object must remain valid
          while fetching. If validatorKeyChain is None, this does not validate
          the data packet.
        :param verifySegment: When a Data packet is received this calls
          verifySegment(data) where data is a Data object. If it returns False then
          abort fetching and call onError with
          SegmentFetcher.ErrorCode.SEGMENT_VERIFICATION_FAILED. If data
          validation is not required, use SegmentFetcher.DontVerifySegment.
        :type verifySegment: function object
        :param onComplete: When all segments are received, call
          onComplete(content) where content is a Blob which has the concatenation of
          the content of all the segments.
          NOTE: The library will log any exceptions raised by this callback, but
          for better error handling the callback should catch and properly
          handle any exceptions.
        :type onComplete: function object
        :param onError: Call onError.onError(errorCode, message) for timeout or an
          error processing segments. errorCode is a value from
          SegmentFetcher.ErrorCode and message is a related string.
          NOTE: The library will log any exceptions raised by this callback, but
          for better error handling the callback should catch and properly
          handle any exceptions.
        :type onError: function object
        """
        # Import KeyChain here to avoid import loops.
        from pyndn.security.key_chain import KeyChain
        if (validatorKeyChainOrVerifySegment == None or
            isinstance(validatorKeyChainOrVerifySegment, KeyChain)):
            TimestampFetcher(
              face, validatorKeyChainOrVerifySegment,
              TimestampFetcher.DontVerifySegment, onComplete,
                onError, initialTimestamp, numIntervals, sampleInterval, originalRequestName)._fetchFirstData(baseInterest)
        else:
            TimestampFetcher(face, None, validatorKeyChainOrVerifySegment,
              onComplete, onError, initialTimestamp, numIntervals, sampleInterval, originalRequestName)._fetchFirstData(baseInterest)

    def _fetchFirstData(self, baseInterest):
        interest = Interest(baseInterest)
        interest.setName(baseInterest.getName().append(Name.Component.fromNumber(self._initialTimestamp)))
        print "Expressing initial timestamped interest: " + interest.getName().toUri()
        self._face.expressInterest(interest, self._onData, self._onTimeout, self._onNetworkNack)

    def _fetchNextData(self, originalInterest, dataName, timestamp):
        # Start with the original Interest to preserve any special selectors.
        interest = Interest(originalInterest)
        # Changing a field clears the nonce so that the library will
        #   generate a new one.
        interest.setMustBeFresh(False)
        interest.setName(dataName.getPrefix(-2).append(Name.Component.fromNumber(timestamp)))
        print "Expressing next interest: " + interest.getName().toUri()
        self._face.expressInterest(interest, self._onData, self._onTimeout, self._onNetworkNack)

    def _onData(self, originalInterest, data):
        if self._validatorKeyChain != None:
            try:
                self._validatorKeyChain.verifyData(
                  data,
                  lambda localData:
                    self._onVerified(localData, originalInterest),
                  self._onValidationFailed)
            except:
                logging.exception("Error in KeyChain.verifyData")
            return
        else:
            if not self._verifySegment(data):
                self._handleError(self.ErrorCode.DATA_VERIFICATION_FAILED, data.getName())
                return

            self._onVerified(data, originalInterest)

    def _onVerified(self, data, originalInterest):
        if not self._endsWithTimestamp(data.getName()):
            # We don't expect a name without a timestamp (number).  Treat it as
            # a bad packet.
            self._handleError(self.ErrorCode.DATA_HAS_NO_TIMESTAMP, data.getName())
        else:
            currentTimestamp = 0
            try:
                currentTimestamp = data.getName().get(-2).toNumber()
                print "Timestamp of verified data: " + str(currentTimestamp)
            except RuntimeError as ex:
                self._handleError(self.ErrorCode.DATA_HAS_NO_TIMESTAMP, data.getName())
                return

            expectedTimestamp = (self._initialTimestamp + ((len(self._retrievedBlobs) + len(self._missingTimestamps)) * self._sampleInterval))
            if currentTimestamp != expectedTimestamp:
              # Try again to get the expected timestamp.  This also includes
              # the case where the first timestamp is not _initialTimestamp.
                self._fetchNextData(
                    originalInterest, data.getName(), expectedTimestamp)
            else:
                # Save the content and check if we are finished.
                self._retrievedBlobs.append(data.getContent())
 
                if currentTimestamp == self._finalTimestamp:

                    print "We reached the final timestamp: " + str(self._finalTimestamp)
                    
                    try:
                        self._onComplete(self._retrievedBlobs, self._originalRequestName)
                    except:
                        logging.exception("Error in onComplete")
                    return

                print "In onData, expected timestamp: " + str(expectedTimestamp)
                
                newTimestamp = expectedTimestamp + self._sampleInterval
                
                print "In onData, new timestamp: " + str(newTimestamp)
                
                # Fetch the next segment.
                self._fetchNextData(
                    originalInterest, data.getName(), newTimestamp)

    def _onValidationFailed(self, data, reason):
        self._handleError(self.ErrorCode.DATA_VERIFICATION_FAILED, data.getName())
        
    def _onTimeout(self, interest):
        self._handleError(self.ErrorCode.INTEREST_TIMEOUT, interest.getName().append("fake_seq_num"))

    def _onNetworkNack(self, interest, reason):
        self._handleError(self.ErrorCode.INTEREST_NACK, interest.getName().append("fake_seq_num"))

    def _handleError(self, errorCode, name):

        print "Name passed into handle error: " + name.toUri()
        
        self._missingTimestamps.append((name.get(-2).toNumber(), errorCode))
        
        oldTimestamp = name.get(-2).toNumber()

        print "Error code " + str(errorCode) + " for timestamp: " + str(oldTimestamp)

        newTimestamp = (self._initialTimestamp + ((len(self._retrievedBlobs) + len(self._missingTimestamps)) * self._sampleInterval))
        
        print "Next timestamp to attempt to fetch: " + str(newTimestamp)

        if newTimestamp <= self._finalTimestamp:
            interest = Interest(name)
            print "Calling fetch next data with interest with name: " + interest.getName().toUri()
            self._fetchNextData(
                interest, interest.getName(), newTimestamp)
        else:
            try:
                self._onError(
                    self._originalRequestName, self._missingTimestamps)
            except:
                logging.exception("Error in onError")

    @staticmethod
    def _endsWithTimestamp(name):
        """
        Check if the last component in the name is a timestamp (number).

        :param Name name: The name to check.
        :return: True if the name ends with a timestamp (number), otherwise False.
        :rtype: bool
        """
        return (name.size() >= 1)# and name.get(-2).isNumber())
