ó
fh[c           @   sV   d  d l  Z  d  d l m Z d  d l m Z d  d l m Z d e f d     YZ d S(   iÿÿÿÿN(   t   Interest(   t   Blob(   t   Namet   TimestampFetcherc           B   s¢   e  Z d  Z d   Z d e f d     YZ e d    Z e d    Z d   Z	 d   Z
 d   Z d	   Z d
   Z d   Z d   Z d   Z e d    Z RS(   s!  
    A private constructor to create a new SegmentFetcher to use the Face. An
    application should use SegmentFetcher.fetch. If validatorKeyChain is not
    None, use it and ignore verifySegment. After creating the SegmentFetcher,
    call fetchFirstSegment.

    :param Face face: This calls face.expressInterest to fetch more segments.
    :param KeyChain validatorKeyChain: If this is not None, use its verifyData
      instead of the verifySegment callback.
    :param verifySegment: When a Data packet is received this calls
      verifySegment(data) where data is a Data object. If it returns False then
      abort fetching and call onError with
      SegmentFetcher.ErrorCode.SEGMENT_VERIFICATION_FAILED.
    :type verifySegment: function object
    :param onComplete: When all segments are received, call
      onComplete(content) where content is a Blob which has the concatenation of
      the content of all the segments.
      NOTE: The library will log any exceptions raised by this callback, but
      for better error handling the callback should catch and properly
      handle any exceptions.
    :type onComplete: function object
    :param onError: Call onError.onError(errorCode, message) for timeout or an
      error processing segments. errorCode is a value from
      SegmentFetcher.ErrorCode and message is a related string.
      NOTE: The library will log any exceptions raised by this callback, but
      for better error handling the callback should catch and properly
      handle any exceptions.
    :type onError: function object
    c
   
      C   sÎ   | |  _  | |  _ | |  _ | |  _ | |  _ | |  _ | |  _ | |  _ |	 |  _ |  j |  j |  j |  _	 d GHd t
 |  j  GHd t
 |  j  GHd t
 |  j  GHd |  j j   GHg  |  _ g  |  _ d  S(   Ns9   Initialized timestamp fetcher with following parameters: s   Initial timestamp: s   Number of intervals: s   Sampling interval: s   Original request name: (   t   _facet   _validatorKeyChaint   _verifySegmentt   _onCompletet   _onErrort   _initialTimestampt   _numIntervalst   _sampleIntervalt   _originalRequestNamet   _finalTimestampt   strt   toUrit   _retrievedBlobst   _missingTimestamps(
   t   selft   facet   validatorKeyChaint   verifySegmentt
   onCompletet   onErrort   initialTimestampt   numIntervalst   sampleIntervalt   originalRequestName(    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyt   __init__%   s"    										t	   ErrorCodec           B   s&   e  Z d  Z d Z d Z d Z d Z RS(   sG   
        An ErrorCode value is passed in the onError callback.
        i   i   i   i   (   t   __name__t
   __module__t   __doc__t   INTEREST_TIMEOUTt   DATA_HAS_NO_TIMESTAMPt   DATA_VERIFICATION_FAILEDt   INTEREST_NACK(    (    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyR   ;   s
   c         C   s   t  S(   sd   
        DontVerifySegment may be used in fetch to skip validation of Data
        packets.
        (   t   True(   t   data(    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyt   DontVerifySegmentD   s    c	   
   
   C   s   d d l  m }	 | d k s+ t | |	  r\ t |  | t j | | | | | | 	 j |  n+ t |  d | | | | | | | 	 j |  d S(   sû	  
        Initiate segment fetching. For more details, see the documentation for
        the module. There are two forms of fetch:
        fetch(face, baseInterest, validatorKeyChain, onComplete, onError)
        and
        fetch(face, baseInterest, verifySegment, onComplete, onError)

        :param Face face: This calls face.expressInterest to fetch more segments.
        :param Interest baseInterest: An Interest for the initial segment of the
          requested data, where baseInterest.getName() has the name prefix.
          This interest may include a custom InterestLifetime and selectors that
          will propagate to all subsequent Interests. The only exception is that
          the initial Interest will be forced to include selectors
          "ChildSelector=1" and "MustBeFresh=true" which will be turned off in
          subsequent Interests.
        :param KeyChain validatorKeyChain: When a Data packet is received this
          calls validatorKeyChain.verifyData(data). If validation fails then
          abort fetching and call onError with SEGMENT_VERIFICATION_FAILED. This
          does not make a copy of the KeyChain; the object must remain valid
          while fetching. If validatorKeyChain is None, this does not validate
          the data packet.
        :param verifySegment: When a Data packet is received this calls
          verifySegment(data) where data is a Data object. If it returns False then
          abort fetching and call onError with
          SegmentFetcher.ErrorCode.SEGMENT_VERIFICATION_FAILED. If data
          validation is not required, use SegmentFetcher.DontVerifySegment.
        :type verifySegment: function object
        :param onComplete: When all segments are received, call
          onComplete(content) where content is a Blob which has the concatenation of
          the content of all the segments.
          NOTE: The library will log any exceptions raised by this callback, but
          for better error handling the callback should catch and properly
          handle any exceptions.
        :type onComplete: function object
        :param onError: Call onError.onError(errorCode, message) for timeout or an
          error processing segments. errorCode is a value from
          SegmentFetcher.ErrorCode and message is a related string.
          NOTE: The library will log any exceptions raised by this callback, but
          for better error handling the callback should catch and properly
          handle any exceptions.
        :type onError: function object
        iÿÿÿÿ(   t   KeyChainN(   t   pyndn.security.key_chainR(   t   Nonet
   isinstanceR   R'   t   _fetchFirstData(
   R   t   baseInterestt    validatorKeyChainOrVerifySegmentR   R   R   R   R   R   R(   (    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyt   fetchL   s    .	c         C   sr   t  |  } | j | j   j t j j |  j    d | j   j   GH|  j	 j
 | |  j |  j |  j  d  S(   Ns)   Expressing initial timestamped interest: (   R    t   setNamet   getNamet   appendR   t	   Componentt
   fromNumberR	   R   R   t   expressInterestt   _onDatat
   _onTimeoutt   _onNetworkNack(   R   R-   t   interest(    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyR,      s    +c         C   s   t  |  } | j t  | j | j d  j t j j |    d | j	   j
   GH|  j j | |  j |  j |  j  d  S(   Niþÿÿÿs   Expressing next interest: (   R    t   setMustBeFresht   FalseR0   t	   getPrefixR2   R   R3   R4   R1   R   R   R5   R6   R7   R8   (   R   t   originalInterestt   dataNamet	   timestampR9   (    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyt   _fetchNextData   s
    +c            s    j  d  k rS y)  j  j |    f d    j  Wn t j d  n Xd  S j |  s  j  j j	 | j
    d  S j |    d  S(   Nc            s    j  |     S(   N(   t   _onVerified(   t	   localData(   R=   R   (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyt   <lambda>   s    s   Error in KeyChain.verifyData(   R   R*   t
   verifyDatat   _onValidationFailedt   loggingt	   exceptionR   t   _handleErrorR   R#   R1   RA   (   R   R=   R&   (    (   R=   R   s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyR6      s    	c         C   s¡  |  j  | j    s4 |  j |  j j | j    nid } y. | j   j d  j   } d t |  GHWn0 t k
 r } |  j |  j j | j    d  SX|  j	 t
 |  j  t
 |  j  |  j } | | k rí |  j | | j   |  n° |  j j | j    | |  j k rYd t |  j  GHy |  j |  j |  j  Wn t j d  n Xd  Sd t |  GH| |  j } d t |  GH|  j | | j   |  d  S(   Ni    iþÿÿÿs   Timestamp of verified data: s    We reached the final timestamp: s   Error in onCompletes   In onData, expected timestamp: s   In onData, new timestamp: (   t   _endsWithTimestampR1   RH   R   R"   t   gett   toNumberR   t   RuntimeErrorR	   t   lenR   R   R   R@   R2   t
   getContentR   R   R   RF   RG   (   R   R&   R=   t   currentTimestampt   ext   expectedTimestampt   newTimestamp(    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyRA   §   s4    *c         C   s    |  j  |  j j | j    d  S(   N(   RH   R   R#   R1   (   R   R&   t   reason(    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyRE   Ó   s    c         C   s)   |  j  |  j j | j   j d   d  S(   Nt   fake_seq_num(   RH   R   R!   R1   R2   (   R   R9   (    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyR7   Ö   s    c         C   s)   |  j  |  j j | j   j d   d  S(   NRT   (   RH   R   R$   R1   R2   (   R   R9   RS   (    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyR8   Ù   s    c         C   s   d | j    GH|  j j | j d  j   | f  | j d  j   } d t |  d t |  GH|  j t |  j  t |  j  |  j	 } d t |  GH| |  j
 k rë t |  } d | j   j    GH|  j | | j   |  n1 y |  j |  j |  j  Wn t j d  n Xd  S(   Ns   Name passed into handle error: iþÿÿÿs   Error code s    for timestamp: s$   Next timestamp to attempt to fetch: s1   Calling fetch next data with interest with name: s   Error in onError(   R   R   R2   RJ   RK   R   R	   RM   R   R   R   R    R1   R@   R   R   RF   RG   (   R   t	   errorCodet   namet   oldTimestampRR   R9   (    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyRH   Ü   s     %*c         C   s   |  j    d k S(   sè   
        Check if the last component in the name is a timestamp (number).

        :param Name name: The name to check.
        :return: True if the name ends with a timestamp (number), otherwise False.
        :rtype: bool
        i   (   t   size(   RV   (    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyRI   ö   s    	(   R   R   R    R   t   objectR   t   staticmethodR'   R/   R,   R@   R6   RA   RE   R7   R8   RH   RI   (    (    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyR      s   		9		
		,				(	   RF   t   pyndn.interestR    t   pyndn.util.blobR   t   pyndnR   RY   R   (    (    (    s2   /home/pi/pi-ndn-ps/aggregator/timestamp_fetcher.pyt   <module>   s   