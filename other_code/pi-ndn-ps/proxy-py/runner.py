
import os
import sys

from threading import Thread

def handler(signal, frame):
    print('Caught keyboard interrupt, exiting...\n')
    global t
    global t2
    global t3
    global t4
    t1 = None
    t2 = None
    t3 = None
    t4 = None
    exit()

def run_chrono_app():

    os.system("sudo chrono-app " + pubsubPrefix + " " + repoName)

def run_repo_ng():

    os.system("sudo ndn-repo-ng");

def run_proxy_app():

    os.system("sudo proxy-py/proxy.py " + pubsubPrefix + " " + repoName)

def run_controller_app():

    os.system("sudo ./onboarder/iot-bootstrapping/controller/linux/controller-sample " +
              pubsubPrefix + " " + repoName)
    
global repoName
repoName = str(sys.argv[2])

global pubsubPrefix
pubsubPrefix = str(sys.argv[1])
    
t1 = Thread(target=run_chrono_app)
t1.daemon = True                            # Daemonize thread
t1.start()

t2 = Thread(target=run_repo_ng)
t2.daemon = True
t2.start()

t3 = Thread(target=run_proxy_app)
t3.daemon = True
t3.start()

t4 = Thread(target=run_controller_app)
t4.daemon = True
t4.start()

signal.signal(signal.SIGINT, handler)

while True:
    t1.join(600)
    t2.join(600)
    t3.join(600)
    t4.join(600)
    if not t1.isAlive():
        print "t1 wasn't alive"
        break
    if not t2.isAlive():
        print "t2 wasn't alive"
        break
    if not t3.isAlive():
        print "t3 wasn't alive"
        break
    if not t4.isAlive():
        print "t4 wasn't alive"
        break
