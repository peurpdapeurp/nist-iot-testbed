#define BOOST_LOG_DYN_LINK 1

#include <iostream>
#include <random>
#include <unistd.h>
#include <fstream>

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/util/scheduler.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include <ndn-cxx/security/signing-helpers.hpp>
#include <ndn-cxx/mgmt/nfd/controller.hpp>
#include <ndn-cxx/security/transform/public-key.hpp>
#include <ndn-cxx/ims/in-memory-storage-persistent.hpp>
#include <ndn-cxx/security/command-interest-signer.hpp>

#include <boost/filesystem.hpp>

#include "../src/repo-command-parameter.hpp"
#include "../src/repo-command-response.hpp"

#include "tinycbor/src/cbor.h"
#include "tinycbor/src/cborencoder.c"
#include "tinycbor/src/cborerrorstrings.c"
#include "tinycbor/src/cborinternal_p.h"
//#include "tinycbor/src/cborjson.h"
#include "tinycbor/src/cborparser.c"
#include "tinycbor/src/cborparser_dup_string.c"
#include "tinycbor/src/cborpretty.c"
#include "tinycbor/src/cborpretty_stdio.c"
//#include "tinycbor/src/cbortojson.c"
#include "tinycbor/src/cborvalidation.c"
#include "tinycbor/src/compilersupport_p.h"
//#include "tinycbor/src/open_memstream.c"
#include "tinycbor/src/tinycbor-version.h"
#include "tinycbor/src/utf8_p.h"

static void shortToByteArray(uint16_t s, uint8_t* buf) {
  buf[0] = (uint8_t) ((s & 0xFF00) >> 8);
  buf[1] = (uint8_t) (s & 0x00FF);
}

static void indent(int nestingLevel)
{
  while (nestingLevel--)
    puts("  ");
}

static void dumpbytes(const uint8_t *buf, size_t len)
{
  while (len--)
    printf("%02X ", *buf++);
}

static bool dumprecursive(CborValue *it, int nestingLevel, std::string *sensorValues)
{
  std::cout << "A call to dump recursive was made with nestingLevel " << nestingLevel << std::endl;

  int counter = 0;
  
  while (!cbor_value_at_end(it)) {
    CborError err;
    CborType type = cbor_value_get_type(it);

    indent(nestingLevel);
    switch (type) {
    case CborArrayType:
    case CborMapType: {
      std::cout << "Cbor type recursive was called in dump recursive." << std::endl;
      // recursive type
      CborValue recursed;
      assert(cbor_value_is_container(it));
      puts(type == CborArrayType ? "Array[" : "Map[");
      err = cbor_value_enter_container(it, &recursed);
      if (err)
	return err;       // parse error
      err = (CborError) dumprecursive(&recursed, nestingLevel + 1, sensorValues);
      if (err)
	return err;       // parse error
      err = cbor_value_leave_container(it, &recursed);
      if (err)
	return err;       // parse error
      indent(nestingLevel);
      puts("]");
      continue;
    }

    case CborIntegerType: {
      int64_t val;
      cbor_value_get_int64(it, &val);     // can't fail
      printf("%lld\n", (long long)val);
      break;
    }

    case CborByteStringType: {
      std::cout << "Cbor byte string type in dump recursive got called." << std::endl;
      uint8_t *buf;
      size_t n;
      err = cbor_value_dup_byte_string(it, &buf, &n, it);
      if (err)
	return err;     // parse error
      dumpbytes(buf, n);
      puts("");
      free(buf);
      continue;
    }

    case CborTextStringType: {
      std::cout << "Cbor text string type in dump recursive got called." << std::endl;
      char *buf;
      size_t n;
      err = cbor_value_dup_text_string(it, &buf, &n, it);
      if (err)
	return err;     // parse error
      std::string value(buf);
      std::cout << "Inserting " << value << " into sensorValues array at index " << counter << std::endl;
      sensorValues[counter] = value;
      std::cout << "Incrementing counter" << std::endl;
      counter++;
      std::cout << "Current value of counter: " << counter << std::endl;
      puts(buf);
      free(buf);
      continue;
    }

    case CborTagType: {
      std::cout << "Cbor tag type in dump recursive got called." << std::endl;
      CborTag tag;
      cbor_value_get_tag(it, &tag);       // can't fail
      printf("Tag(%lld)\n", (long long)tag);
      break;
    }

    case CborSimpleType: {
      std::cout << "Cbor simple type in dump recursive got called." << std::endl;
      uint8_t type;
      cbor_value_get_simple_type(it, &type);  // can't fail
      printf("simple(%u)\n", type);
      break;
    }

    case CborNullType:
      puts("null");
      break;

    case CborUndefinedType:
      puts("undefined");
      break;

    case CborBooleanType: {
      bool val;
      cbor_value_get_boolean(it, &val);       // can't fail
      puts(val ? "true" : "false");
      break;
    }

    case CborDoubleType: {
      double val;
      if (false) {
	float f;
      case CborFloatType:
	cbor_value_get_float(it, &f);
	val = f;
      } else {
	cbor_value_get_double(it, &val);
      }
      printf("%g\n", val);
      break;
    }
    case CborHalfFloatType: {
      uint16_t val;
      cbor_value_get_half_float(it, &val);
      printf("__f16(%04x)\n", val);
      break;
    }

    case CborInvalidType:
      assert(false);      // can't happen
      break;
    }

    err = cbor_value_advance_fixed(it);
    if (err)
      return err;
  }
  return CborNoError;
}

using namespace ndn;
using namespace repo;

class DeviceSigner
{
public:
  DeviceSigner(std::string deviceName, std::string prefix, std::string repoName)
    : m_scheduler(m_face.getIoService())
    , m_deviceName(deviceName)
      // /<BigCompany>/<Building1>/<ConfRoom>/sensor/<sensorName>/<sensorType>/<timestamp>
    , m_prefix(Name(prefix).append(repoName).append(m_deviceName)) // Key Name prefix
    , m_sensorI(Name(m_deviceName)) // Data Name
    , m_repoPrefix(Name("localhost").append(repoName))
    , m_seqFileName("/home/pi/repo-ng/seq/")
    , m_cmdSigner(m_keyChain)
  {

    m_request_frequency = 30000;
    uint64_t currentTime = duration_cast< milliseconds >(ndn::time::system_clock::now().time_since_epoch()).count();
    std::cout << "The current time: " << currentTime << std::endl;
    m_next_timestamp = (currentTime - (currentTime % m_request_frequency)) + m_request_frequency;
    std::cout << "The current m_next_timestamp: " << m_next_timestamp << std::endl;
    
    m_seqFileName.append(m_deviceName);
    m_seqFileName.append(".seq");
    initiateSeqFromFile();    

    Name registerName("dc-exp");
    registerName.append(m_prefix);

    std::cout << "Dcexp register name: " << registerName.toUri() << std::endl;

    /*
    m_face.setInterestFilter(registerName,
			     bind(&DeviceSigner::onInterestFromRepo, this, _1, _2),
			     [this] (const Name& prefix) {
			       std::cout << "Prefix: " << prefix << " successfully registered." << std::endl;
			       runScheduler();
			     },
			     [] (const ndn::Name& prefix, const std::string& reason) {
			       std::cerr << "Register failed: " << reason << std::endl;
			     },
			     security::SigningInfo(),
			     nfd::ROUTE_FLAG_CAPTURE);
    */
    
    m_face.registerPrefix(registerName,
			  [this] (const Name& prefix) {
			    std::cout << "Prefix: " << prefix << " successfully registered." << std::endl;
			    runScheduler();
			  },
			  [] (const ndn::Name& prefix, const std::string& reason) {
			    std::cerr << "Register failed: " << reason << std::endl;
			  },
			  security::SigningInfo(),
			  nfd::ROUTE_FLAG_CAPTURE);

    m_face.setInterestFilter(InterestFilter(m_prefix),
			     bind(&DeviceSigner::onInterestFromRepo, this, _1, _2));

    uint8_t temperature[2];
    uint8_t humidity[2];
    uint8_t pressure[2];
    uint8_t gasResistance[2];
    shortToByteArray(4097, temperature);
    shortToByteArray(4098, humidity);
    shortToByteArray(4099, pressure);
    shortToByteArray(4100, gasResistance);
    uint8_t sensorTypesByteArray[8];
    memcpy(sensorTypesByteArray, temperature, 2);
    memcpy(sensorTypesByteArray + 2, humidity, 2);
    memcpy(sensorTypesByteArray + 4, pressure, 2);
    memcpy(sensorTypesByteArray + 6, gasResistance, 2);

    for (int i = 0; i < 8; i++) {
      std::cout << "Sensor types byte array index " << i << ": " << unsigned(sensorTypesByteArray[i]) << std::endl;
    }
    
    Block sensorTypesBlock();
    m_sensorI.append(sensorTypesByteArray, 8);

    std::cout << "Value of m_sensorI: " + m_sensorI.toUri() << std::endl;
  }

  void runScheduler() {    
    uint64_t currentTime = duration_cast< milliseconds >(ndn::time::system_clock::now().time_since_epoch()).count();

    std::cout << "Time at run scheduler call: " << currentTime << std::endl;
    
    if (currentTime < m_next_timestamp) {
      struct timespec tim, tim2;
      uint64_t time_to_wait = m_next_timestamp - duration_cast< milliseconds >(ndn::time::system_clock::now().time_since_epoch()).count();
      tim.tv_sec = (time_to_wait - (time_to_wait % 1000)) / 1000;
      tim.tv_nsec = (time_to_wait % 1000) * 100000;

      std::cout << "Seconds to wait: " << ((time_to_wait - (time_to_wait % 1000)) / 1000) << std::endl;
      std::cout << "Nano seconds to wait: " << ((time_to_wait % 1000) * 100000) << std::endl;
      
      nanosleep(&tim, &tim2);
    }
    
    m_next_timestamp += m_request_frequency;
    std::cout << "New next timestamp: " << m_next_timestamp << std::endl;
    uint64_t remaining_time_until_next_timestamp = m_next_timestamp - duration_cast< milliseconds >(ndn::time::system_clock::now().time_since_epoch()).count();
    std::cout << "Time to wait until next timestamp: " << remaining_time_until_next_timestamp << std::endl;
    time::steady_clock::Duration wait_time = ndn::time::milliseconds(remaining_time_until_next_timestamp);
    
    sendInterest(m_sensorI);  
    
    m_scheduler.scheduleEvent(wait_time, std::bind(&DeviceSigner::runScheduler, this));
 
  }

  void
  initiateSeqFromFile() {
      std::ifstream inputFile(m_seqFileName.c_str());
      if (inputFile.good()) {
	std::string sensorSeq;
	uint32_t seqNo = 0;

	inputFile >> sensorSeq >> seqNo;
	m_tempSeq = seqNo;

	inputFile >> sensorSeq >> seqNo;
	m_humidSeq = seqNo;

	inputFile >> sensorSeq >> seqNo;
	m_pressureSeq = seqNo;

	inputFile >> sensorSeq >> seqNo;
	m_resistanceSeq = seqNo;
	
      } else {
	writeSeqToFile();
      }
      inputFile.close();
    }

  void writeSeqToFile() {
    std::ofstream outputFile(m_seqFileName.c_str());
    std::ostringstream os;
    uint64_t lastTimestamp = m_next_timestamp - m_request_frequency;
    os << "4097 " << std::to_string(m_tempSeq) << " " << std::to_string(lastTimestamp) << "\n"
       << "4098 "  << std::to_string(m_humidSeq)  << " " << std::to_string(lastTimestamp) << "\n"
       << "4099 "  << std::to_string(m_pressureSeq) << " " << std::to_string(lastTimestamp) << "\n"
       << "4100 " << std::to_string(m_resistanceSeq) << " " << std::to_string(lastTimestamp);
    outputFile << os.str();
    outputFile.close();
  }

  void
  sendInterest(const Name& interestName)
  {
    std::cout << "Sending interest for : " << interestName.toUri() << std::endl;

    Interest interest(interestName);
    interest.setMustBeFresh(true);
    
    m_face.expressInterest(interest,
			   bind(&DeviceSigner::onData, this,  _1, _2),
			   [] (const Interest& interest, const ndn::lp::Nack& nack) {
                	     std::cout << "Nack for interest sent from the sendInterest function: "  << interest.getName().toUri() << ": " << nack.getReason() << std::endl;
                           },
                           [] (const Interest& interest) {
                	     std::cout << "Timeout for interest sent from the sendInterest function: " << interest.getName().toUri() << std::endl;
                           });
    

  }

  void
  onData(const Interest& interest, const Data& data)
  {

    std::cout << "Got data for interest: " + interest.getName().toUri() << std::endl;

    Block dataContentBlock = data.getContent();
    size_t length = dataContentBlock.value_size();
    const uint8_t* buf = dataContentBlock.value();

    std::cout << "Length of value of content of data: " << length << std::endl;

    int numSensorTypes = (interest.getName().get(-1).value_size())/2;

    std::cout << "Number of sensor types: " << numSensorTypes << std::endl;

    std::string sensorValues[numSensorTypes];
    CborParser parser;
    CborValue it;
    CborError err = cbor_parser_init(buf, length, 0, &parser, &it);
    if (!err)
      err = (CborError) dumprecursive(&it, 0, sensorValues);
    //free(buf);

    if (err) {
      fprintf(stderr, "CBOR parsing failure at offset %ld: %s\n",
	      it.ptr - buf, cbor_error_string(err));
    }

    std::cout << "Values in sensor types array:" << std::endl;
    for (int i = 0; i < numSensorTypes; i++) {
      std::cout << sensorValues[i] << std::endl;
    }

    /*
    Name dataName(m_prefix);

    if (data.getName() == m_temperatureI) {
      dataName.append("temperature");
      dataName.appendNumber(m_next_timestamp - m_request_frequency);
      dataName.appendNumber(m_tempSeq++);
    }
    else if (data.getName() == m_humidityI) {
      dataName.append("humidity");
      dataName.appendNumber(m_next_timestamp - m_request_frequency);
      dataName.appendNumber(m_humidSeq++);
    }
    else if (data.getName() == m_pressureI) {
      dataName.append("pressure");
      dataName.appendNumber(m_next_timestamp - m_request_frequency);
      dataName.appendNumber(m_pressureSeq++);
    }
    else if (data.getName() == m_resistanceI) {
      dataName.append("resistance");
      dataName.appendNumber(m_next_timestamp - m_request_frequency);
      dataName.appendNumber(m_resistanceSeq++);
    }
    writeSeqToFile();

    // Prepare data to be inserted into repo
    std::shared_ptr<Data> repoData = std::make_shared<Data>();
    // /uofm/dunn-hall/sensor/221/temperature/%00
    repoData->setName(dataName);
    repoData->setContent(data.getContent());
    repoData->setFreshnessPeriod(time::milliseconds(1000));

    // sign by identity: /uofm/dunn-hall/221/sensor/panel1/
    m_keyChain.sign(*repoData);

    //std::cout << "Inserting: " << repoData->getName() << std::endl;
    std::cout << "Inserting into repo: " << repoData->getName() << std::endl;

    insertIntoRepo(repoData);

    // Schedule fetch
    //time::steady_clock::Duration after = ndn::time::milliseconds(m_dis(m_mt));

    */

    Name dataNameTemp(m_prefix);
    Name dataNameHumid(m_prefix);
    Name dataNamePressure(m_prefix);
    Name dataNameResistance(m_prefix);
 
    dataNameTemp.appendNumber(4097);
    dataNameTemp.appendNumber(m_next_timestamp - m_request_frequency);
    dataNameTemp.appendNumber(m_tempSeq++);
  
    dataNameHumid.appendNumber(4098);
    dataNameHumid.appendNumber(m_next_timestamp - m_request_frequency);
    dataNameHumid.appendNumber(m_humidSeq++);
    
    dataNamePressure.appendNumber(4099);
    dataNamePressure.appendNumber(m_next_timestamp - m_request_frequency);
    dataNamePressure.appendNumber(m_pressureSeq++);
    
    dataNameResistance.appendNumber(4100);
    dataNameResistance.appendNumber(m_next_timestamp - m_request_frequency);
    dataNameResistance.appendNumber(m_resistanceSeq++);
   
    writeSeqToFile();

    // Prepare data to be inserted into repo
    std::shared_ptr<Data> repoDataTemp = std::make_shared<Data>();
    std::shared_ptr<Data> repoDataHumid = std::make_shared<Data>();
    std::shared_ptr<Data> repoDataPressure = std::make_shared<Data>();
    std::shared_ptr<Data> repoDataResistance = std::make_shared<Data>();
    // /uofm/dunn-hall/sensor/221/temperature/%00
    repoDataTemp->setName(dataNameTemp);
    repoDataTemp->setContent(data.wireEncode());
    repoDataTemp->setFreshnessPeriod(time::milliseconds(1000));

    repoDataHumid->setName(dataNameHumid);
    repoDataHumid->setContent(data.wireEncode());
    repoDataHumid->setFreshnessPeriod(time::milliseconds(1000));

    repoDataPressure->setName(dataNamePressure);
    repoDataPressure->setContent(data.wireEncode());
    repoDataPressure->setFreshnessPeriod(time::milliseconds(1000));

    repoDataResistance->setName(dataNameResistance);
    repoDataResistance->setContent(data.wireEncode());
    repoDataResistance->setFreshnessPeriod(time::milliseconds(1000));

    // sign by identity: /uofm/dunn-hall/221/sensor/panel1/
    m_keyChain.sign(*repoDataTemp);
    m_keyChain.sign(*repoDataHumid);
    m_keyChain.sign(*repoDataPressure);
    m_keyChain.sign(*repoDataResistance);
    
    //std::cout << "Inserting: " << repoData->getName() << std::endl;
    std::cout << "Inserting into repo: " << repoDataTemp->getName() << std::endl;
    std::cout << "Inserting into repo: " << repoDataHumid->getName() << std::endl;
    std::cout << "Inserting into repo: " << repoDataPressure->getName() << std::endl;
    std::cout << "Inserting into repo: " << repoDataResistance->getName() << std::endl;
    
    insertIntoRepo(repoDataTemp);
    insertIntoRepo(repoDataHumid);
    insertIntoRepo(repoDataPressure);
    insertIntoRepo(repoDataResistance);

  }

  void
  insertIntoRepo(const std::shared_ptr<Data> data) {
      // Insert into in memory persistent storage
      m_ims.insert(*data);

      RepoCommandParameter parameters;
      parameters.setName(data->getName());

      // Generate command interest
      Interest interest;
      Name cmd = m_repoPrefix;
          cmd
	    .append("insert")
	    .append(parameters.wireEncode());

	  interest = m_cmdSigner.makeCommandInterest(cmd);

	  interest.setInterestLifetime(time::milliseconds(4000));

	  m_face.expressInterest(interest,
				 [] (const Interest& interest, const Data& data) {
				   std::cout << "Data from repo insert interest: " << interest.getName().toUri() << std::endl;
				 },
				 [] (const Interest& interest, const ndn::lp::Nack& nack) {
				   std::cout << "Nack from repo " << nack.getReason() << "for interest: " << interest.getName().toUri() << std::endl;
				 },
				 [] (const Interest& interest) {
				   std::cout << "Timeout from repo for interest: " << interest.getName().toUri() << std::endl;
				 });
  }
  
  void
  onInterestFromRepo(const ndn::Name& prefix, const ndn::Interest& interest) {

      std::shared_ptr<const Data> data = m_ims.find(interest.getName());

      if (!data) {
	std::cout << "Data not found in IMS! " << interest.getName() << std::endl;
	return;
      }

      //m_ims.
      std::cout << "Sending data to repo: " << data->getName() << std::endl;

      m_face.put(*data);
  }

  void run()
  {
    try {
      m_face.processEvents();
    } catch (std::runtime_error& e) {
      std::cerr << e.what() << std::endl;
      return;
    }
  }

private:
  Face m_face;
  Scheduler m_scheduler;
  KeyChain m_keyChain;

  // Device name is used to create a face to the device
  // Prefix is the prefix of the data name that the device is listening to
  std::string m_deviceName;
  Name m_prefix;
  Name m_sensorI;
  uint32_t m_tempSeq = 0, m_humidSeq = 0, m_pressureSeq = 0, m_resistanceSeq = 0;

  ndn::InMemoryStoragePersistent m_ims;
  ndn::Name m_repoPrefix;
  std::string m_seqFileName;
  ndn::security::CommandInterestSigner m_cmdSigner;

  uint64_t m_request_frequency;
  uint64_t m_next_timestamp;
};

int main(int argc, char* argv[]) {
  if ( argc != 4 ) {
    std::cout << " Usage: " << argv[0]
	      << " <deviceName> <prefix - /Company/building/roomNumber> <repoName>\n";
  }
  else {
    std::string deviceName(argv[1]);
    std::string prefix(argv[2]);
    std::string repoName(argv[3]);
    DeviceSigner ds(deviceName, prefix, repoName);
    ds.run();
  }
}
