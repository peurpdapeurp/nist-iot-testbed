
function timestampSecondsIncrementer(timestamp, increment) {
	
		// will assume the timestamp will be formatted like: Y2018m07d02H19M00S34
		
		if (increment > 60) {
			log("ERROR: Increment in timestampSecondsIncrementer cannot be greater than 60.");
			return;
		}
	
		var timestampString = timestamp;
	
		var sec = parseInt(timestampString.substring(18));
		var min = parseInt(timestampString.substring(15, 17));
		var hour = parseInt(timestampString.substring(12, 14));
		var day = parseInt(timestampString.substring(9, 11));
		var month = parseInt(timestampString.substring(6, 8));
		var year = parseInt(timestampString.substring(1, 5));
		
		//contentElement.innerHTML = "";
		//contentElement.innerHTML += "<br>" + (year) + "<br>" + (month) + "<br>" + (day) + "<br>" + (hour) + "<br>" + (min) + "<br>" + (sec);
		
		sec += increment;
		if (sec > 59) {
			sec -= 60;
			min += 1;
		}
		if (min > 59) {
			min -= 60;
			hour += 1;
		}
		if (hour > 23) {
			hour -= 24;
			day += 1;
		}
		switch (month) {
		case 1:
			if (day > 31) {
				day -= 31;
				month += 1;
			}
			break;
		case 2:
			if (day > 28) {
				day -= 28;
				month += 1;
			}
			break;
		case 3:
			if (day > 31) {
				day -= 31;
				month += 1;
			}
			break;
		case 4:
			if (day > 30) {
				day -= 30;
				month += 1;
			}
			break;
		case 5:
			if (day > 31) {
				day -= 31;
				month += 1;
			}
			break;
		case 6:
			if (day > 30) {
				day -= 30;
				month += 1;
			}
			break;
		case 7:
			if (day > 31) {
				
				day -= 31;
				month += 1;
			}
			break;
		case 8:
			if (day > 31) {
				day -= 31;
				month += 1;
			}
			break;
		case 9:
			if (day > 30) {
				day -= 30;
				month += 1;
			}
			break;
		case 10:
			if (day > 31) {
				day -= 31;
				month += 1;
			}
			break;
		case 11:
			if (day > 30) {
				day -= 30;
				month += 1;
			}
			break;
		case 12:
			if (day > 31) {
				day -= 31;
				month += 1;
			}
			break;
		}
		if (month > 12) {
			month -= 12;
			year += 1;
		}
		
		return intsToTimestamp(year, month, day, hour, min, sec);
	}
	
	function intsToTimestamp(year, month, day, hour, min, sec) {
	
		var yearString = year.toString();
		var monthString;
		if (month < 10) {
			monthString = "0" + month.toString();
		}
		else {
			monthString = month.toString();
		}
		var dayString;
		if (day < 10) {
			dayString = "0" + day.toString();
		}
		else {
			dayString = day.toString();
		}
		var hourString;
		if (hour < 10) {
			hourString = "0" + hour.toString();
		}
		else {
			hourString = hour.toString();
		}
		var minString;
		if (min < 10) {
			minString = "0" + min.toString();
		}
		else {
			minString = min.toString();
		}
		var secString;
		if (sec < 10) {
			secString = "0" + sec.toString();
		}
		else {
			secString = sec.toString();
		}
		
		//updateContent("<br>" + "Y2018m12d30H23M59S59" +
		//"<br>" + "Y" + (yearString) + "m" + (monthString) + "d" + (dayString)+ "H" + (hourString) + "M" + (minString) + "S" + (secString));
		
		var finalString = "Y" + (yearString) + "m" + (monthString) + "d" + (dayString)+ "H" + (hourString) + "M" + (minString) + "S" + (secString);
		
		return finalString;
	}
	
	// this function will return 1 if timestamp1 is later than timestamp2
	// returns 0 if timestamp1 and timestamp2 are equal
	// returns -1 if timestamp1 is earlier than timestamp2
	function compareTimestamps(timestamp1, timestamp2) {
		
		if (timestamp1 == timestamp2)
			return 0;
		
		var sec1 = parseInt(timestamp1.substring(18));
		var min1 = parseInt(timestamp1.substring(15, 17));
		var hour1 = parseInt(timestamp1.substring(12, 14));
		var day1 = parseInt(timestamp1.substring(9, 11));
		var month1 = parseInt(timestamp1.substring(6, 8));
		var year1 = parseInt(timestamp1.substring(1, 5));
		
		var sec2 = parseInt(timestamp2.substring(18));
		var min2 = parseInt(timestamp2.substring(15, 17));
		var hour2 = parseInt(timestamp2.substring(12, 14));
		var day2 = parseInt(timestamp2.substring(9, 11));
		var month2 = parseInt(timestamp2.substring(6, 8));
		var year2 = parseInt(timestamp2.substring(1, 5));
		
		
		if (year1 > year2)
			return 1;
		if (month1 > month2 && year1 >= year2)
			return 1;
		if (day1 > day2 && month1 >= month2 && year1 >= year2)
			return 1;
		if (hour1 > hour2 && day1 >= day2 && month1 >= month2 && year1 >= year2)
			return 1;
		if (min1 > min2 && hour1 >= hour2 && day1 >= day2 && month1 >= month2 && year1 >= year2)
			return 1;
		if (sec1 > sec2 && min1 >= min2 && hour1 >= hour2 && day1 >= day2 && month1 >= month2 && year1 >= year2)
			return 1;
		
		return -1;
	}